# Phantom

Resources for our [Pi Wars 2019](https://piwars.org/2019-competition/) robot, [Phantom](https://bldrbts.me.uk/met-2019/).

Still work in progress…

- [`pizerow_setup.md`: Configuring the Pi Zero W](pizerow_setup.md)
- [`hardware`](hardware): Schematics and PCBs
- [`python_gui`](python_gui): Python GUI tools – see below

## GUI Tools

[`python_gui`](python_gui): tools for controlling/testing/simulating Phantom.
This is very much still work in progress. For more details, see comments in the source.

![phantom_sim](python_gui/phantom_sim.png)

### Prerequisites

    apt-get install python-virtualenv python3-virtualenv python3-tk python3-dev

If installing on a Raspberry Pi, this is also needed:

    apt-get install libatlas-base-dev

Also needs the following modules:

- [`control_logic`](https://gitlab.com/marsextraterrestrials/control_logic)
- [`pygame_utils`](https://gitlab.com/marsextraterrestrials/pygame_utils)

These should be checked out in folders at the same level as this one, so `control_logic`, `pygame_utils` and `phantom` next to each other.

### Setup

    virtualenv -p python3 --system-site-packages python_gui_env
    source python_gui_env/bin/activate
    pip install -r python_gui/requirements.txt
    
### Usage

(Assuming the required modules are checked out as above – if not, use a different path for `PYTHONPATH`.)

    source python_gui_env/bin/activate
    
    # Run the simulator:
    PYTHONPATH=.. python -m python_gui.phantom_sim

