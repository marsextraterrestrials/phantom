import pygame
from pygame.locals import *
from sim import run
from utils import *
from construct import create_robot

WORLD_SIZE = (1000, 600)
motors = []    
speed_inc = 0

def init_space(space):
    global motors
    
    # walls (left-right)
    add_wall(space, (0, 0), (0, WORLD_SIZE[1]))
    add_wall(space, (WORLD_SIZE[0], 0), WORLD_SIZE)

    for i in range(0,3):
        # floors
        add_wall(space, (0, i*200), (WORLD_SIZE[0], i*200))
        # slopes
        add_wall(space, (WORLD_SIZE[0]/3*2, i*200), (WORLD_SIZE[0], i*200+50))

    # original (2019 version)
    robot1, motor1 = create_robot(space, (110, 60))

    # new version with smaller steps
    robot2, motor2 = create_robot(space, (110, 260),
                                  crank_length = 2.2,
                                  mid_bar_inner_length = 7.2,
                                  bottom_bar_length = 4,
                                  mid_bar_outer_length = 6,
                                  leg_top_length = 12,
                                  top_bar_length = 5,
                                  leg_bottom_length = 15,
                                  leg_angle = 22)
    
    # changing leg_bottom_length only
    robot3, motor3 = create_robot(space, (110, 480),
                                  crank_length =  2.2,
                                  mid_bar_inner_length = 7.2,
                                  bottom_bar_length = 4,
                                  mid_bar_outer_length = 6,
                                  leg_top_length = 12,
                                  top_bar_length = 5,
                                  leg_bottom_length = 20,
                                  leg_angle = 22)
    
    motors = [motor1, motor2, motor3]


def process_event(event):
    global speed_inc
    
    if event.type == KEYDOWN:
        if event.key == pygame.K_RIGHT:
            speed_inc = 0.1
        elif event.key == pygame.K_LEFT:
            speed_inc = -0.1
    elif event.type == pygame.KEYUP:
        if event.key == pygame.K_RIGHT or event.key == pygame.K_LEFT:
            speed_inc = 0
            
def update():
    global motors, speed_inc
    
    if speed_inc != 0:
        for motor in motors:
            motor.rate = max(-20, min(20, motor.rate + speed_inc))
        
run(WORLD_SIZE, init_space, process_event, update)        
