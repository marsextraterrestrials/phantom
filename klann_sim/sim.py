import sys
import pygame
from pygame.locals import *
import pymunk
import pygame_util_ext

def run(world_size, init_space, process_event, update):
    """
    Run the main loop.
    :param world_size: size of the whole world (can be much bigger than the screen)
    :param init_space: init_space(space): callback to add object to the space or do any other initialisation, before the main loop
    :param process_event: process_event(<pygame event>): called with any pygame event from the main loop to add custom keyboard etc. handling
    :param update: update(): callback for updates after event handling and before rendering
    """
    pygame.init()
    screen = pygame.display.set_mode((1400, 800))
    pygame.display.set_caption("M.E.T. Phantom Klann mechanism simulation")
    clock = pygame.time.Clock()

    space = pymunk.Space()
    space.damping = 0.9
    space.gravity = (0.0, -9000.0)

    init_space(space)

    draw_options = pygame_util_ext.DrawOptions(screen)
    pygame_util_ext.scale = 1.4

    def limit_translate():
        w, h = screen.get_size()
        limit = world_size[0] * pygame_util_ext.scale - w, world_size[1] * pygame_util_ext.scale - h
        pygame_util_ext.translate = min(max(-limit[0], pygame_util_ext.translate[0]), 0), min(max(-limit[1], pygame_util_ext.translate[1]), 0)
    
    running = False
    speed_inc = 0
    FPS = 50
    ITERATIONS = 100
    while True:
        for event in pygame.event.get():
            if event.type == QUIT:
                sys.exit(0)
            elif event.type == KEYDOWN:
                if event.key == K_q:
                    sys.exit(0)
                elif event.key == K_SPACE:
                    running = not running
            elif event.type == pygame.JOYBUTTONDOWN and event.button == 1 or event.type == pygame.MOUSEBUTTONDOWN and event.button == 4:
                pygame_util_ext.scale = min(7, pygame_util_ext.scale + 0.1)
                limit_translate()
            elif event.type == pygame.JOYBUTTONDOWN and event.button == 2 or event.type == pygame.MOUSEBUTTONDOWN and event.button == 5:
                pygame_util_ext.scale = max(0.1, pygame_util_ext.scale - 0.1)
                limit_translate()
            elif event.type == pygame.MOUSEMOTION and event.buttons[2] == 1:
                pygame_util_ext.translate = pygame_util_ext.translate[0] + event.rel[0], pygame_util_ext.translate[1] - event.rel[1]
                limit_translate()
            process_event(event)
                
        if running:
            update()
            dt = 1.0 / FPS / ITERATIONS
            for x in range(ITERATIONS):
                space.step(dt)
            
        screen.fill((0,0,0))
        space.debug_draw(draw_options)

        pygame.display.flip()
        clock.tick(FPS)
