import math
import pymunk.pygame_util
from pymunk.vec2d import Vec2d

WALL_COLOUR = (100, 50, 50)

DEFAULT_DENSITY = 0.1
DEFAULT_FRICTION = 1

def circle_intersection(p1, p2, r1, r2, negative=False):
    d = p2.get_distance(p1)
    x = -(r2**2 - r1**2 - d**2) / (2*d)
    y = math.sqrt(abs(r1**2 - x**2))
    if negative:
        y = -y
    ip = Vec2d(x, y)
    ip.rotate((p2 - p1).get_angle())
    return ip + p1

def add_wall(space, a, b):
    body = pymunk.Body(body_type = pymunk.Body.STATIC)
    l = pymunk.Segment(body, a, b, 5)
    l.color = WALL_COLOUR
    l.friction = DEFAULT_FRICTION * 10
    space.add(l)

def add_poly(space, vertices, shape_filter, pos = (0,0), density = DEFAULT_DENSITY, friction = DEFAULT_FRICTION):
    body = pymunk.Body()
    body.position = pos
    shape = pymunk.Poly(body, vertices)
    shape.density = density
    shape.friction = friction
    space.add(body, shape)
    shape.filter = shape_filter
    shape.color = (80, 80, 80, 0)
    return shape

def add_bar(space, a, b, density = DEFAULT_DENSITY, friction = DEFAULT_FRICTION):
    body = pymunk.Body()
    shape = pymunk.Segment(body, a, b, 3)
    shape.density = density
    shape.friction = friction
    shape.color = (200, 200, 200)
    space.add(body, shape)
    return shape

def join_link(space, first, link, p = None):    
    j = pymunk.PivotJoint(first.body, link.body, link.a if p is None else p)
    j.collide_bodies = False
    space.add(j)

def add_gear(space, frame, radius, shape_filter, pos = (0,0), density = DEFAULT_DENSITY, friction = DEFAULT_FRICTION):
    body = pymunk.Body()
    body.position = pos
    shape = pymunk.Circle(body, radius)
    shape.density = density
    shape.friction = friction
    shape.color = (100, 100, 100, 0)
    shape.filter = shape_filter
    space.add(body, shape)
    j = pymunk.PivotJoint(frame.body, body, pos)
    space.add(j)
    return shape
    
