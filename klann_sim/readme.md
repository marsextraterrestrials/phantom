# Klann Mechanism Simulator

This is a simple Python simulator to test the [Klann linkage](https://en.wikipedia.org/wiki/Klann_linkage) in 2D,
using the [Pymunk](https://www.pymunk.org) physics engine to replicate the leg mechanism used by Phantom.

The goal of this is not a fully detailed or 100% accurate simulation: the focus is only on the leg mechanism, to be able to test movement, speed, gait with various configurations using different sizes for the links and legs.

It contains tools to “assemble” a robot (with one center motor driving two gears that in turn drive the linkage for four legs), and to let it move around in a simple simulated “world”.

![phantom_sim](test3.png)

## Setup

To set up virtualenv, dependencies, etc.:

    make
    
## Usage

Two simple test programs are available:

1. Test one robot with some obstacles:

        make run-obstacles

2. Test three robots in parallel, each using a different configuration:

        make run-test3
    
Control:

- Use the mouse wheel to zoom in/out
- Drag with the right mouse button to pan the viewport
- Left-right arrows to change motor speed
- Space to start/pause/resume
- Q to quit
