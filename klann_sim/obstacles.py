import pygame
from pygame.locals import *
from sim import run
from utils import *
from construct import create_robot

WORLD_SIZE = (2000, 600)
motor = None    
speed_inc = 0


def init_space(space):
    global motor
    
    # walls (left-right)
    add_wall(space, (0, 0), (0, WORLD_SIZE[1]))
    add_wall(space, (WORLD_SIZE[0], 0), WORLD_SIZE)
    # floor
    add_wall(space, (0, 0), (WORLD_SIZE[0], 0))

    # slope down
    add_wall(space, (0, 100), (WORLD_SIZE[0]/6, 0))

    # bumps
    for i in range(0,4):
        add_wall(space, (WORLD_SIZE[0]/4 + i*80, 0), (WORLD_SIZE[0]/4+40 + i*80, 10))
        add_wall(space, (WORLD_SIZE[0]/4+40 + i*80, 10), (WORLD_SIZE[0]/4+80 + i*80, 0))

    # slope up
    add_wall(space, (WORLD_SIZE[0]/4*2, 0), (WORLD_SIZE[0]/4*2+100, 50))
    add_wall(space, (WORLD_SIZE[0]/4*2+100, 50), (WORLD_SIZE[0]/4*2+300, 50))
    # and down
    add_wall(space, (WORLD_SIZE[0]/4*2+300, 50), (WORLD_SIZE[0]/4*2+300+200, 0))
    
    robot, motor = create_robot(space, (110, 160))

    
def process_event(event):
    global speed_inc
    
    if event.type == KEYDOWN:
        if event.key == pygame.K_RIGHT:
            speed_inc = 0.1
        elif event.key == pygame.K_LEFT:
            speed_inc = -0.1
    elif event.type == pygame.KEYUP:
        if event.key == pygame.K_RIGHT or event.key == pygame.K_LEFT:
            speed_inc = 0
            
def update():
    global motor, speed_inc
    
    if speed_inc != 0:
        motor.rate = max(-20, min(20, motor.rate + speed_inc))
        
run(WORLD_SIZE, init_space, process_event, update)        
