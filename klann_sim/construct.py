import pymunk
from pymunk.vec2d import Vec2d

from utils import *

#
# To scale the original "standard" lengths from https://www.diywalkers.com/klanns-linkage-plans.html to
# actual sizes (used in the CAD designs). 
#
KLANN_SCALE = 10.0 / 3


def create_robot(space,
                 pos = (0,0),
                 # Frame attachment point distances (relative to a crank gear axis)
                 klann_top_x = 3,
                 klann_top_y = 7,
                 klann_bottom_x = 7,
                 klann_bottom_y = -2,
                 # Klann bar lengths
                 crank_length = 3,          # crank
                 mid_bar_inner_length = 7,  # first half of bar from crank
                 bottom_bar_length = 4,     # bar connecting middle point of bar from crank to frame
                 mid_bar_outer_length = 6,  # second half of bar from crank
                 leg_top_length = 10,       # top part of leg
                 top_bar_length = 6,        # bar from top of leg to frame
                 leg_bottom_length = 15,    # bottom part of leg
                 leg_angle = 15             # angle between top/bottom of leg
):

    # Gear parameters
    motor_gear_diameter = 17.5
    crank_gear_diameter = 40
    gear_ratio = 2.5
    
    
    def add_crank(space, crank_dir, gear, gear_p, shape_filter):
        crank_end_p = gear_p + (crank_dir * crank_length * KLANN_SCALE, 0)
        crank_bar = add_bar(space, gear_p, crank_end_p)
        crank_bar.filter = shape_filter
        join_link(space, gear, crank_bar)
        j = pymunk.GearJoint(gear.body, crank_bar.body, 0, 1)
        space.add(j)

        return crank_bar


    def add_leg(space, frame, crank_bar, top_p, bottom_p, shape_filter, front=True, outer=True):
        mid_dir = -1 if front else 1

        crank_end_p = crank_bar.b

        middle_mid_p = circle_intersection(crank_end_p, bottom_p, mid_bar_inner_length * KLANN_SCALE, bottom_bar_length * KLANN_SCALE, negative=front)
        middle_end_p = (middle_mid_p - crank_end_p) * (1 + mid_bar_outer_length * KLANN_SCALE / (mid_bar_inner_length * KLANN_SCALE)) + crank_end_p
        middle_bar = add_bar(space, crank_end_p, middle_end_p)
        middle_bar.filter = shape_filter
        join_link(space, crank_bar, middle_bar)

        top_end_p = circle_intersection(top_p, middle_end_p, top_bar_length * KLANN_SCALE, leg_top_length * KLANN_SCALE, front)
        top_bar = add_bar(space, top_p, top_end_p)
        top_bar.filter = shape_filter
        join_link(space, frame, top_bar)

        leg_top = add_bar(space, top_end_p, middle_end_p)
        leg_top.filter = shape_filter
        join_link(space, leg_top, top_bar, top_end_p)
        join_link(space, leg_top, middle_bar, middle_end_p)

        leg_bottom_p = (middle_end_p - top_end_p) * (1 + leg_bottom_length * KLANN_SCALE / (leg_top_length * KLANN_SCALE))
        leg_bottom_p.rotate(-mid_dir * leg_angle * 2 * math.pi / 360)
        leg_bottom_p = leg_bottom_p + top_end_p
        shape = pymunk.Segment(leg_top.body, middle_end_p, leg_bottom_p, 3)
        shape.density = DEFAULT_DENSITY
        shape.friction = DEFAULT_FRICTION
        shape.filter = shape_filter
        space.add(shape)

        bottom_bar = add_bar(space, bottom_p, middle_mid_p)
        bottom_bar.filter = shape_filter
        join_link(space, frame, bottom_bar)

        join_link(space, bottom_bar, middle_bar, middle_mid_p)


    def assemble(space, pos = (0,0)):
        crank_gear_dist = motor_gear_diameter/2 + crank_gear_diameter/2
        shape_filter = pymunk.ShapeFilter(group=1)

        # attachment points (relative to middle of frame)
        top_left = Vec2d(-crank_gear_dist - klann_top_x * KLANN_SCALE, klann_top_y * KLANN_SCALE)
        top_right = Vec2d(crank_gear_dist + klann_top_x * KLANN_SCALE, klann_top_y * KLANN_SCALE)
        bottom_right = Vec2d(crank_gear_dist + klann_bottom_x * KLANN_SCALE, klann_bottom_y * KLANN_SCALE)
        bottom_left = Vec2d(-crank_gear_dist - klann_bottom_x * KLANN_SCALE, klann_bottom_y * KLANN_SCALE)

        # gear positions (absolute)
        left_gear_p = pos + Vec2d(-crank_gear_dist, 0)
        right_gear_p = pos + Vec2d(crank_gear_dist, 0)

        frame = add_poly(space, [ top_left+(-5,5), top_right+(5,5), bottom_right+(5,-5), bottom_left+(-5,-5) ], shape_filter, pos)

        motor_gear = add_gear(space, frame, motor_gear_diameter/2, shape_filter, pos)

        left_crank_gear = add_gear(space, frame, crank_gear_diameter/2, shape_filter, left_gear_p)
        j = pymunk.constraint.GearJoint(motor_gear.body, left_crank_gear.body, 0, gear_ratio)
        space.add(j)

        right_crank_gear = add_gear(space, frame, crank_gear_diameter/2, shape_filter, right_gear_p)
        j = pymunk.constraint.GearJoint(motor_gear.body, right_crank_gear.body, 0, gear_ratio)
        space.add(j)

        motor = pymunk.SimpleMotor(motor_gear.body, frame.body, 3)
        space.add(motor)

        crank_bar = add_crank(space, -1, left_crank_gear, left_gear_p, shape_filter)
        add_leg(space, frame, crank_bar, top_left + pos, bottom_left + pos, shape_filter, front=True, outer=True)
        crank_bar = add_crank(space, 1, left_crank_gear, left_gear_p, shape_filter)
        add_leg(space, frame, crank_bar, top_left + pos, bottom_left + pos, shape_filter, front=True, outer=False)

        crank_bar = add_crank(space, -1, right_crank_gear, right_gear_p, shape_filter)
        add_leg(space, frame, crank_bar, top_right + pos, bottom_right + pos, shape_filter, front=False, outer=True)
        crank_bar = add_crank(space, 1, right_crank_gear, right_gear_p, shape_filter)
        add_leg(space, frame, crank_bar, top_right + pos, bottom_right + pos, shape_filter, front=False, outer=False)

        return frame.body, motor
    
    return assemble(space, pos)
