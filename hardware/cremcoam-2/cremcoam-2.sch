<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="6.6.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="pinhead">
<description>&lt;b&gt;Pin Header Connectors&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="2X20">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-25.4" y1="-1.905" x2="-24.765" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-24.765" y1="-2.54" x2="-23.495" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-23.495" y1="-2.54" x2="-22.86" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-22.86" y1="-1.905" x2="-22.225" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-22.225" y1="-2.54" x2="-20.955" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-20.955" y1="-2.54" x2="-20.32" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-20.32" y1="-1.905" x2="-19.685" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-19.685" y1="-2.54" x2="-18.415" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-18.415" y1="-2.54" x2="-17.78" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-17.78" y1="-1.905" x2="-17.145" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-17.145" y1="-2.54" x2="-15.875" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-15.875" y1="-2.54" x2="-15.24" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-15.24" y1="-1.905" x2="-14.605" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-14.605" y1="-2.54" x2="-13.335" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-2.54" x2="-12.7" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="-1.905" x2="-12.065" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-12.065" y1="-2.54" x2="-10.795" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-10.795" y1="-2.54" x2="-10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-25.4" y1="-1.905" x2="-25.4" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-25.4" y1="1.905" x2="-24.765" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-24.765" y1="2.54" x2="-23.495" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-23.495" y1="2.54" x2="-22.86" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-22.86" y1="1.905" x2="-22.225" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-22.225" y1="2.54" x2="-20.955" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-20.955" y1="2.54" x2="-20.32" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-20.32" y1="1.905" x2="-19.685" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-19.685" y1="2.54" x2="-18.415" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-18.415" y1="2.54" x2="-17.78" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-17.78" y1="1.905" x2="-17.145" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-17.145" y1="2.54" x2="-15.875" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-15.875" y1="2.54" x2="-15.24" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-15.24" y1="1.905" x2="-14.605" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-14.605" y1="2.54" x2="-13.335" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="2.54" x2="-12.7" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.065" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-12.065" y1="2.54" x2="-10.795" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-10.795" y1="2.54" x2="-10.16" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="1.905" x2="-9.525" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-9.525" y1="2.54" x2="-8.255" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="2.54" x2="-7.62" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-6.985" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="2.54" x2="-5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="2.54" x2="-5.08" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.905" x2="-4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="2.54" x2="-3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="2.54" x2="-2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="2.54" x2="-0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="0" y2="1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="1.905" x2="0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="2.54" x2="1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="2.54" x2="2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.905" x2="3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="2.54" x2="4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="5.08" y1="1.905" x2="4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="5.08" y1="1.905" x2="5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.985" y1="2.54" x2="5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.985" y1="2.54" x2="7.62" y2="1.905" width="0.1524" layer="21"/>
<wire x1="7.62" y1="1.905" x2="8.255" y2="2.54" width="0.1524" layer="21"/>
<wire x1="9.525" y1="2.54" x2="8.255" y2="2.54" width="0.1524" layer="21"/>
<wire x1="9.525" y1="2.54" x2="10.16" y2="1.905" width="0.1524" layer="21"/>
<wire x1="10.16" y1="1.905" x2="10.795" y2="2.54" width="0.1524" layer="21"/>
<wire x1="12.065" y1="2.54" x2="10.795" y2="2.54" width="0.1524" layer="21"/>
<wire x1="12.065" y1="2.54" x2="12.7" y2="1.905" width="0.1524" layer="21"/>
<wire x1="12.7" y1="1.905" x2="13.335" y2="2.54" width="0.1524" layer="21"/>
<wire x1="14.605" y1="2.54" x2="13.335" y2="2.54" width="0.1524" layer="21"/>
<wire x1="14.605" y1="2.54" x2="15.24" y2="1.905" width="0.1524" layer="21"/>
<wire x1="15.24" y1="1.905" x2="15.875" y2="2.54" width="0.1524" layer="21"/>
<wire x1="17.145" y1="2.54" x2="15.875" y2="2.54" width="0.1524" layer="21"/>
<wire x1="17.145" y1="2.54" x2="17.78" y2="1.905" width="0.1524" layer="21"/>
<wire x1="17.78" y1="1.905" x2="18.415" y2="2.54" width="0.1524" layer="21"/>
<wire x1="19.685" y1="2.54" x2="18.415" y2="2.54" width="0.1524" layer="21"/>
<wire x1="19.685" y1="2.54" x2="20.32" y2="1.905" width="0.1524" layer="21"/>
<wire x1="20.32" y1="1.905" x2="20.955" y2="2.54" width="0.1524" layer="21"/>
<wire x1="22.225" y1="2.54" x2="20.955" y2="2.54" width="0.1524" layer="21"/>
<wire x1="22.225" y1="2.54" x2="22.86" y2="1.905" width="0.1524" layer="21"/>
<wire x1="22.86" y1="-1.905" x2="22.225" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="22.225" y1="-2.54" x2="20.955" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-1.905" x2="20.955" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-1.905" x2="19.685" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="19.685" y1="-2.54" x2="18.415" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="17.78" y1="-1.905" x2="18.415" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="17.78" y1="-1.905" x2="17.145" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="17.145" y1="-2.54" x2="15.875" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-1.905" x2="15.875" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-1.905" x2="14.605" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="13.335" y1="-2.54" x2="14.605" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="13.335" y1="-2.54" x2="12.7" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="12.7" y1="-1.905" x2="12.065" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="10.795" y1="-2.54" x2="12.065" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="10.795" y1="-2.54" x2="10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-1.905" x2="9.525" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="9.525" y1="-2.54" x2="8.255" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-1.905" x2="8.255" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-1.905" x2="6.985" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-2.54" x2="5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-2.54" x2="3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-2.54" x2="0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="-0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="-1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-2.54" x2="-4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-2.54" x2="-6.985" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="-1.905" x2="-6.985" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="-1.905" x2="-8.255" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="-2.54" x2="-9.525" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-1.905" x2="-9.525" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-22.86" y1="1.905" x2="-22.86" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-20.32" y1="1.905" x2="-20.32" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-17.78" y1="1.905" x2="-17.78" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-15.24" y1="1.905" x2="-15.24" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.7" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="1.905" x2="-10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.905" x2="-5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="1.905" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.905" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="5.08" y1="1.905" x2="5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="7.62" y1="1.905" x2="7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="10.16" y1="1.905" x2="10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="12.7" y1="1.905" x2="12.7" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="15.24" y1="1.905" x2="15.24" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="17.78" y1="1.905" x2="17.78" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="20.32" y1="1.905" x2="20.32" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="22.86" y1="1.905" x2="22.86" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="22.86" y1="1.905" x2="23.495" y2="2.54" width="0.1524" layer="21"/>
<wire x1="24.765" y1="2.54" x2="23.495" y2="2.54" width="0.1524" layer="21"/>
<wire x1="24.765" y1="2.54" x2="25.4" y2="1.905" width="0.1524" layer="21"/>
<wire x1="25.4" y1="-1.905" x2="24.765" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="24.765" y1="-2.54" x2="23.495" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="22.86" y1="-1.905" x2="23.495" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="25.4" y1="1.905" x2="25.4" y2="-1.905" width="0.1524" layer="21"/>
<pad name="1" x="-24.13" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="2" x="-24.13" y="1.27" drill="1.016" shape="octagon"/>
<pad name="3" x="-21.59" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="4" x="-21.59" y="1.27" drill="1.016" shape="octagon"/>
<pad name="5" x="-19.05" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="6" x="-19.05" y="1.27" drill="1.016" shape="octagon"/>
<pad name="7" x="-16.51" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="8" x="-16.51" y="1.27" drill="1.016" shape="octagon"/>
<pad name="9" x="-13.97" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="10" x="-13.97" y="1.27" drill="1.016" shape="octagon"/>
<pad name="11" x="-11.43" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="12" x="-11.43" y="1.27" drill="1.016" shape="octagon"/>
<pad name="13" x="-8.89" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="14" x="-8.89" y="1.27" drill="1.016" shape="octagon"/>
<pad name="15" x="-6.35" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="16" x="-6.35" y="1.27" drill="1.016" shape="octagon"/>
<pad name="17" x="-3.81" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="18" x="-3.81" y="1.27" drill="1.016" shape="octagon"/>
<pad name="19" x="-1.27" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="20" x="-1.27" y="1.27" drill="1.016" shape="octagon"/>
<pad name="21" x="1.27" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="22" x="1.27" y="1.27" drill="1.016" shape="octagon"/>
<pad name="23" x="3.81" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="24" x="3.81" y="1.27" drill="1.016" shape="octagon"/>
<pad name="25" x="6.35" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="26" x="6.35" y="1.27" drill="1.016" shape="octagon"/>
<pad name="27" x="8.89" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="28" x="8.89" y="1.27" drill="1.016" shape="octagon"/>
<pad name="29" x="11.43" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="30" x="11.43" y="1.27" drill="1.016" shape="octagon"/>
<pad name="31" x="13.97" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="32" x="13.97" y="1.27" drill="1.016" shape="octagon"/>
<pad name="33" x="16.51" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="34" x="16.51" y="1.27" drill="1.016" shape="octagon"/>
<pad name="35" x="19.05" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="36" x="19.05" y="1.27" drill="1.016" shape="octagon"/>
<pad name="37" x="21.59" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="38" x="21.59" y="1.27" drill="1.016" shape="octagon"/>
<pad name="39" x="24.13" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="40" x="24.13" y="1.27" drill="1.016" shape="octagon"/>
<text x="-25.4" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-25.4" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-24.384" y1="-1.524" x2="-23.876" y2="-1.016" layer="51"/>
<rectangle x1="-24.384" y1="1.016" x2="-23.876" y2="1.524" layer="51"/>
<rectangle x1="-21.844" y1="1.016" x2="-21.336" y2="1.524" layer="51"/>
<rectangle x1="-21.844" y1="-1.524" x2="-21.336" y2="-1.016" layer="51"/>
<rectangle x1="-19.304" y1="1.016" x2="-18.796" y2="1.524" layer="51"/>
<rectangle x1="-19.304" y1="-1.524" x2="-18.796" y2="-1.016" layer="51"/>
<rectangle x1="-16.764" y1="1.016" x2="-16.256" y2="1.524" layer="51"/>
<rectangle x1="-14.224" y1="1.016" x2="-13.716" y2="1.524" layer="51"/>
<rectangle x1="-11.684" y1="1.016" x2="-11.176" y2="1.524" layer="51"/>
<rectangle x1="-16.764" y1="-1.524" x2="-16.256" y2="-1.016" layer="51"/>
<rectangle x1="-14.224" y1="-1.524" x2="-13.716" y2="-1.016" layer="51"/>
<rectangle x1="-11.684" y1="-1.524" x2="-11.176" y2="-1.016" layer="51"/>
<rectangle x1="-9.144" y1="1.016" x2="-8.636" y2="1.524" layer="51"/>
<rectangle x1="-9.144" y1="-1.524" x2="-8.636" y2="-1.016" layer="51"/>
<rectangle x1="-6.604" y1="1.016" x2="-6.096" y2="1.524" layer="51"/>
<rectangle x1="-6.604" y1="-1.524" x2="-6.096" y2="-1.016" layer="51"/>
<rectangle x1="-4.064" y1="1.016" x2="-3.556" y2="1.524" layer="51"/>
<rectangle x1="-4.064" y1="-1.524" x2="-3.556" y2="-1.016" layer="51"/>
<rectangle x1="-1.524" y1="1.016" x2="-1.016" y2="1.524" layer="51"/>
<rectangle x1="-1.524" y1="-1.524" x2="-1.016" y2="-1.016" layer="51"/>
<rectangle x1="1.016" y1="1.016" x2="1.524" y2="1.524" layer="51"/>
<rectangle x1="1.016" y1="-1.524" x2="1.524" y2="-1.016" layer="51"/>
<rectangle x1="3.556" y1="1.016" x2="4.064" y2="1.524" layer="51"/>
<rectangle x1="3.556" y1="-1.524" x2="4.064" y2="-1.016" layer="51"/>
<rectangle x1="6.096" y1="1.016" x2="6.604" y2="1.524" layer="51"/>
<rectangle x1="6.096" y1="-1.524" x2="6.604" y2="-1.016" layer="51"/>
<rectangle x1="8.636" y1="1.016" x2="9.144" y2="1.524" layer="51"/>
<rectangle x1="8.636" y1="-1.524" x2="9.144" y2="-1.016" layer="51"/>
<rectangle x1="11.176" y1="1.016" x2="11.684" y2="1.524" layer="51"/>
<rectangle x1="11.176" y1="-1.524" x2="11.684" y2="-1.016" layer="51"/>
<rectangle x1="13.716" y1="1.016" x2="14.224" y2="1.524" layer="51"/>
<rectangle x1="13.716" y1="-1.524" x2="14.224" y2="-1.016" layer="51"/>
<rectangle x1="16.256" y1="1.016" x2="16.764" y2="1.524" layer="51"/>
<rectangle x1="16.256" y1="-1.524" x2="16.764" y2="-1.016" layer="51"/>
<rectangle x1="18.796" y1="1.016" x2="19.304" y2="1.524" layer="51"/>
<rectangle x1="18.796" y1="-1.524" x2="19.304" y2="-1.016" layer="51"/>
<rectangle x1="21.336" y1="1.016" x2="21.844" y2="1.524" layer="51"/>
<rectangle x1="21.336" y1="-1.524" x2="21.844" y2="-1.016" layer="51"/>
<rectangle x1="23.876" y1="1.016" x2="24.384" y2="1.524" layer="51"/>
<rectangle x1="23.876" y1="-1.524" x2="24.384" y2="-1.016" layer="51"/>
</package>
<package name="2X20/90">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-25.4" y1="-1.905" x2="-22.86" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-22.86" y1="-1.905" x2="-22.86" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-22.86" y1="0.635" x2="-25.4" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-25.4" y1="0.635" x2="-25.4" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-24.13" y1="6.985" x2="-24.13" y2="1.27" width="0.762" layer="21"/>
<wire x1="-22.86" y1="-1.905" x2="-20.32" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-20.32" y1="-1.905" x2="-20.32" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-20.32" y1="0.635" x2="-22.86" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-21.59" y1="6.985" x2="-21.59" y2="1.27" width="0.762" layer="21"/>
<wire x1="-20.32" y1="-1.905" x2="-17.78" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-17.78" y1="-1.905" x2="-17.78" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-17.78" y1="0.635" x2="-20.32" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-19.05" y1="6.985" x2="-19.05" y2="1.27" width="0.762" layer="21"/>
<wire x1="-17.78" y1="-1.905" x2="-15.24" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-15.24" y1="-1.905" x2="-15.24" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-15.24" y1="0.635" x2="-17.78" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-16.51" y1="6.985" x2="-16.51" y2="1.27" width="0.762" layer="21"/>
<wire x1="-15.24" y1="-1.905" x2="-12.7" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="-1.905" x2="-12.7" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="0.635" x2="-15.24" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-13.97" y1="6.985" x2="-13.97" y2="1.27" width="0.762" layer="21"/>
<wire x1="-12.7" y1="-1.905" x2="-10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-1.905" x2="-10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="0.635" x2="-12.7" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-11.43" y1="6.985" x2="-11.43" y2="1.27" width="0.762" layer="21"/>
<wire x1="-10.16" y1="-1.905" x2="-7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="-1.905" x2="-7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0.635" x2="-10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-8.89" y1="6.985" x2="-8.89" y2="1.27" width="0.762" layer="21"/>
<wire x1="-7.62" y1="-1.905" x2="-5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="6.985" x2="-6.35" y2="1.27" width="0.762" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="6.985" x2="-3.81" y2="1.27" width="0.762" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="6.985" x2="-1.27" y2="1.27" width="0.762" layer="21"/>
<wire x1="0" y1="-1.905" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="6.985" x2="1.27" y2="1.27" width="0.762" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0.635" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="6.985" x2="3.81" y2="1.27" width="0.762" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-1.905" x2="7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="7.62" y1="0.635" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="6.35" y1="6.985" x2="6.35" y2="1.27" width="0.762" layer="21"/>
<wire x1="7.62" y1="-1.905" x2="10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-1.905" x2="10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="10.16" y1="0.635" x2="7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="8.89" y1="6.985" x2="8.89" y2="1.27" width="0.762" layer="21"/>
<wire x1="10.16" y1="-1.905" x2="12.7" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="12.7" y1="-1.905" x2="12.7" y2="0.635" width="0.1524" layer="21"/>
<wire x1="12.7" y1="0.635" x2="10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="11.43" y1="6.985" x2="11.43" y2="1.27" width="0.762" layer="21"/>
<wire x1="12.7" y1="-1.905" x2="15.24" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-1.905" x2="15.24" y2="0.635" width="0.1524" layer="21"/>
<wire x1="15.24" y1="0.635" x2="12.7" y2="0.635" width="0.1524" layer="21"/>
<wire x1="13.97" y1="6.985" x2="13.97" y2="1.27" width="0.762" layer="21"/>
<wire x1="15.24" y1="-1.905" x2="17.78" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="17.78" y1="-1.905" x2="17.78" y2="0.635" width="0.1524" layer="21"/>
<wire x1="17.78" y1="0.635" x2="15.24" y2="0.635" width="0.1524" layer="21"/>
<wire x1="16.51" y1="6.985" x2="16.51" y2="1.27" width="0.762" layer="21"/>
<wire x1="17.78" y1="-1.905" x2="20.32" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-1.905" x2="20.32" y2="0.635" width="0.1524" layer="21"/>
<wire x1="20.32" y1="0.635" x2="17.78" y2="0.635" width="0.1524" layer="21"/>
<wire x1="19.05" y1="6.985" x2="19.05" y2="1.27" width="0.762" layer="21"/>
<wire x1="20.32" y1="-1.905" x2="22.86" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="22.86" y1="-1.905" x2="22.86" y2="0.635" width="0.1524" layer="21"/>
<wire x1="22.86" y1="0.635" x2="20.32" y2="0.635" width="0.1524" layer="21"/>
<wire x1="21.59" y1="6.985" x2="21.59" y2="1.27" width="0.762" layer="21"/>
<wire x1="22.86" y1="-1.905" x2="25.4" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="25.4" y1="-1.905" x2="25.4" y2="0.635" width="0.1524" layer="21"/>
<wire x1="25.4" y1="0.635" x2="22.86" y2="0.635" width="0.1524" layer="21"/>
<wire x1="24.13" y1="6.985" x2="24.13" y2="1.27" width="0.762" layer="21"/>
<pad name="2" x="-24.13" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="4" x="-21.59" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="6" x="-19.05" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="8" x="-16.51" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="10" x="-13.97" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="12" x="-11.43" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="14" x="-8.89" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="16" x="-6.35" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="18" x="-3.81" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="20" x="-1.27" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="22" x="1.27" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="24" x="3.81" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="26" x="6.35" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="28" x="8.89" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="30" x="11.43" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="32" x="13.97" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="34" x="16.51" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="36" x="19.05" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="38" x="21.59" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="1" x="-24.13" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="3" x="-21.59" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="5" x="-19.05" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="7" x="-16.51" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="9" x="-13.97" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="11" x="-11.43" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="13" x="-8.89" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="15" x="-6.35" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="17" x="-3.81" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="19" x="-1.27" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="21" x="1.27" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="23" x="3.81" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="25" x="6.35" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="27" x="8.89" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="29" x="11.43" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="31" x="13.97" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="33" x="16.51" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="35" x="19.05" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="37" x="21.59" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="40" x="24.13" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="39" x="24.13" y="-6.35" drill="1.016" shape="octagon"/>
<text x="-26.035" y="-3.81" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="27.305" y="-3.81" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-24.511" y1="0.635" x2="-23.749" y2="1.143" layer="21"/>
<rectangle x1="-21.971" y1="0.635" x2="-21.209" y2="1.143" layer="21"/>
<rectangle x1="-19.431" y1="0.635" x2="-18.669" y2="1.143" layer="21"/>
<rectangle x1="-16.891" y1="0.635" x2="-16.129" y2="1.143" layer="21"/>
<rectangle x1="-14.351" y1="0.635" x2="-13.589" y2="1.143" layer="21"/>
<rectangle x1="-11.811" y1="0.635" x2="-11.049" y2="1.143" layer="21"/>
<rectangle x1="-9.271" y1="0.635" x2="-8.509" y2="1.143" layer="21"/>
<rectangle x1="-6.731" y1="0.635" x2="-5.969" y2="1.143" layer="21"/>
<rectangle x1="-4.191" y1="0.635" x2="-3.429" y2="1.143" layer="21"/>
<rectangle x1="-1.651" y1="0.635" x2="-0.889" y2="1.143" layer="21"/>
<rectangle x1="0.889" y1="0.635" x2="1.651" y2="1.143" layer="21"/>
<rectangle x1="3.429" y1="0.635" x2="4.191" y2="1.143" layer="21"/>
<rectangle x1="5.969" y1="0.635" x2="6.731" y2="1.143" layer="21"/>
<rectangle x1="8.509" y1="0.635" x2="9.271" y2="1.143" layer="21"/>
<rectangle x1="11.049" y1="0.635" x2="11.811" y2="1.143" layer="21"/>
<rectangle x1="13.589" y1="0.635" x2="14.351" y2="1.143" layer="21"/>
<rectangle x1="16.129" y1="0.635" x2="16.891" y2="1.143" layer="21"/>
<rectangle x1="18.669" y1="0.635" x2="19.431" y2="1.143" layer="21"/>
<rectangle x1="21.209" y1="0.635" x2="21.971" y2="1.143" layer="21"/>
<rectangle x1="23.749" y1="0.635" x2="24.511" y2="1.143" layer="21"/>
<rectangle x1="-24.511" y1="-2.921" x2="-23.749" y2="-1.905" layer="21"/>
<rectangle x1="-21.971" y1="-2.921" x2="-21.209" y2="-1.905" layer="21"/>
<rectangle x1="-24.511" y1="-5.461" x2="-23.749" y2="-4.699" layer="21"/>
<rectangle x1="-24.511" y1="-4.699" x2="-23.749" y2="-2.921" layer="51"/>
<rectangle x1="-21.971" y1="-4.699" x2="-21.209" y2="-2.921" layer="51"/>
<rectangle x1="-21.971" y1="-5.461" x2="-21.209" y2="-4.699" layer="21"/>
<rectangle x1="-19.431" y1="-2.921" x2="-18.669" y2="-1.905" layer="21"/>
<rectangle x1="-16.891" y1="-2.921" x2="-16.129" y2="-1.905" layer="21"/>
<rectangle x1="-19.431" y1="-5.461" x2="-18.669" y2="-4.699" layer="21"/>
<rectangle x1="-19.431" y1="-4.699" x2="-18.669" y2="-2.921" layer="51"/>
<rectangle x1="-16.891" y1="-4.699" x2="-16.129" y2="-2.921" layer="51"/>
<rectangle x1="-16.891" y1="-5.461" x2="-16.129" y2="-4.699" layer="21"/>
<rectangle x1="-14.351" y1="-2.921" x2="-13.589" y2="-1.905" layer="21"/>
<rectangle x1="-14.351" y1="-5.461" x2="-13.589" y2="-4.699" layer="21"/>
<rectangle x1="-14.351" y1="-4.699" x2="-13.589" y2="-2.921" layer="51"/>
<rectangle x1="-11.811" y1="-2.921" x2="-11.049" y2="-1.905" layer="21"/>
<rectangle x1="-9.271" y1="-2.921" x2="-8.509" y2="-1.905" layer="21"/>
<rectangle x1="-11.811" y1="-5.461" x2="-11.049" y2="-4.699" layer="21"/>
<rectangle x1="-11.811" y1="-4.699" x2="-11.049" y2="-2.921" layer="51"/>
<rectangle x1="-9.271" y1="-4.699" x2="-8.509" y2="-2.921" layer="51"/>
<rectangle x1="-9.271" y1="-5.461" x2="-8.509" y2="-4.699" layer="21"/>
<rectangle x1="-6.731" y1="-2.921" x2="-5.969" y2="-1.905" layer="21"/>
<rectangle x1="-4.191" y1="-2.921" x2="-3.429" y2="-1.905" layer="21"/>
<rectangle x1="-6.731" y1="-5.461" x2="-5.969" y2="-4.699" layer="21"/>
<rectangle x1="-6.731" y1="-4.699" x2="-5.969" y2="-2.921" layer="51"/>
<rectangle x1="-4.191" y1="-4.699" x2="-3.429" y2="-2.921" layer="51"/>
<rectangle x1="-4.191" y1="-5.461" x2="-3.429" y2="-4.699" layer="21"/>
<rectangle x1="-1.651" y1="-2.921" x2="-0.889" y2="-1.905" layer="21"/>
<rectangle x1="-1.651" y1="-5.461" x2="-0.889" y2="-4.699" layer="21"/>
<rectangle x1="-1.651" y1="-4.699" x2="-0.889" y2="-2.921" layer="51"/>
<rectangle x1="0.889" y1="-2.921" x2="1.651" y2="-1.905" layer="21"/>
<rectangle x1="3.429" y1="-2.921" x2="4.191" y2="-1.905" layer="21"/>
<rectangle x1="0.889" y1="-5.461" x2="1.651" y2="-4.699" layer="21"/>
<rectangle x1="0.889" y1="-4.699" x2="1.651" y2="-2.921" layer="51"/>
<rectangle x1="3.429" y1="-4.699" x2="4.191" y2="-2.921" layer="51"/>
<rectangle x1="3.429" y1="-5.461" x2="4.191" y2="-4.699" layer="21"/>
<rectangle x1="5.969" y1="-2.921" x2="6.731" y2="-1.905" layer="21"/>
<rectangle x1="8.509" y1="-2.921" x2="9.271" y2="-1.905" layer="21"/>
<rectangle x1="5.969" y1="-5.461" x2="6.731" y2="-4.699" layer="21"/>
<rectangle x1="5.969" y1="-4.699" x2="6.731" y2="-2.921" layer="51"/>
<rectangle x1="8.509" y1="-4.699" x2="9.271" y2="-2.921" layer="51"/>
<rectangle x1="8.509" y1="-5.461" x2="9.271" y2="-4.699" layer="21"/>
<rectangle x1="11.049" y1="-2.921" x2="11.811" y2="-1.905" layer="21"/>
<rectangle x1="11.049" y1="-5.461" x2="11.811" y2="-4.699" layer="21"/>
<rectangle x1="11.049" y1="-4.699" x2="11.811" y2="-2.921" layer="51"/>
<rectangle x1="13.589" y1="-2.921" x2="14.351" y2="-1.905" layer="21"/>
<rectangle x1="16.129" y1="-2.921" x2="16.891" y2="-1.905" layer="21"/>
<rectangle x1="13.589" y1="-5.461" x2="14.351" y2="-4.699" layer="21"/>
<rectangle x1="13.589" y1="-4.699" x2="14.351" y2="-2.921" layer="51"/>
<rectangle x1="16.129" y1="-4.699" x2="16.891" y2="-2.921" layer="51"/>
<rectangle x1="16.129" y1="-5.461" x2="16.891" y2="-4.699" layer="21"/>
<rectangle x1="18.669" y1="-2.921" x2="19.431" y2="-1.905" layer="21"/>
<rectangle x1="21.209" y1="-2.921" x2="21.971" y2="-1.905" layer="21"/>
<rectangle x1="18.669" y1="-5.461" x2="19.431" y2="-4.699" layer="21"/>
<rectangle x1="18.669" y1="-4.699" x2="19.431" y2="-2.921" layer="51"/>
<rectangle x1="21.209" y1="-4.699" x2="21.971" y2="-2.921" layer="51"/>
<rectangle x1="21.209" y1="-5.461" x2="21.971" y2="-4.699" layer="21"/>
<rectangle x1="23.749" y1="-2.921" x2="24.511" y2="-1.905" layer="21"/>
<rectangle x1="23.749" y1="-5.461" x2="24.511" y2="-4.699" layer="21"/>
<rectangle x1="23.749" y1="-4.699" x2="24.511" y2="-2.921" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="PINH2X20">
<wire x1="-6.35" y1="-27.94" x2="8.89" y2="-27.94" width="0.4064" layer="94"/>
<wire x1="8.89" y1="-27.94" x2="8.89" y2="25.4" width="0.4064" layer="94"/>
<wire x1="8.89" y1="25.4" x2="-6.35" y2="25.4" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="25.4" x2="-6.35" y2="-27.94" width="0.4064" layer="94"/>
<text x="-6.35" y="26.035" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-30.48" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="22.86" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="5.08" y="22.86" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="3" x="-2.54" y="20.32" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="5.08" y="20.32" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="5" x="-2.54" y="17.78" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="6" x="5.08" y="17.78" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="7" x="-2.54" y="15.24" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="8" x="5.08" y="15.24" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="9" x="-2.54" y="12.7" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="10" x="5.08" y="12.7" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="11" x="-2.54" y="10.16" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="12" x="5.08" y="10.16" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="13" x="-2.54" y="7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="14" x="5.08" y="7.62" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="15" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="16" x="5.08" y="5.08" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="17" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="18" x="5.08" y="2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="19" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="20" x="5.08" y="0" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="21" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="22" x="5.08" y="-2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="23" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="24" x="5.08" y="-5.08" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="25" x="-2.54" y="-7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="26" x="5.08" y="-7.62" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="27" x="-2.54" y="-10.16" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="28" x="5.08" y="-10.16" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="29" x="-2.54" y="-12.7" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="30" x="5.08" y="-12.7" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="31" x="-2.54" y="-15.24" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="32" x="5.08" y="-15.24" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="33" x="-2.54" y="-17.78" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="34" x="5.08" y="-17.78" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="35" x="-2.54" y="-20.32" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="36" x="5.08" y="-20.32" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="37" x="-2.54" y="-22.86" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="38" x="5.08" y="-22.86" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="39" x="-2.54" y="-25.4" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="40" x="5.08" y="-25.4" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PINHD-2X20" prefix="JP" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="PINH2X20" x="0" y="0"/>
</gates>
<devices>
<device name="" package="2X20">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="10" pad="10"/>
<connect gate="A" pin="11" pad="11"/>
<connect gate="A" pin="12" pad="12"/>
<connect gate="A" pin="13" pad="13"/>
<connect gate="A" pin="14" pad="14"/>
<connect gate="A" pin="15" pad="15"/>
<connect gate="A" pin="16" pad="16"/>
<connect gate="A" pin="17" pad="17"/>
<connect gate="A" pin="18" pad="18"/>
<connect gate="A" pin="19" pad="19"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="20" pad="20"/>
<connect gate="A" pin="21" pad="21"/>
<connect gate="A" pin="22" pad="22"/>
<connect gate="A" pin="23" pad="23"/>
<connect gate="A" pin="24" pad="24"/>
<connect gate="A" pin="25" pad="25"/>
<connect gate="A" pin="26" pad="26"/>
<connect gate="A" pin="27" pad="27"/>
<connect gate="A" pin="28" pad="28"/>
<connect gate="A" pin="29" pad="29"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="30" pad="30"/>
<connect gate="A" pin="31" pad="31"/>
<connect gate="A" pin="32" pad="32"/>
<connect gate="A" pin="33" pad="33"/>
<connect gate="A" pin="34" pad="34"/>
<connect gate="A" pin="35" pad="35"/>
<connect gate="A" pin="36" pad="36"/>
<connect gate="A" pin="37" pad="37"/>
<connect gate="A" pin="38" pad="38"/>
<connect gate="A" pin="39" pad="39"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="40" pad="40"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
<connect gate="A" pin="8" pad="8"/>
<connect gate="A" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/90" package="2X20/90">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="10" pad="10"/>
<connect gate="A" pin="11" pad="11"/>
<connect gate="A" pin="12" pad="12"/>
<connect gate="A" pin="13" pad="13"/>
<connect gate="A" pin="14" pad="14"/>
<connect gate="A" pin="15" pad="15"/>
<connect gate="A" pin="16" pad="16"/>
<connect gate="A" pin="17" pad="17"/>
<connect gate="A" pin="18" pad="18"/>
<connect gate="A" pin="19" pad="19"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="20" pad="20"/>
<connect gate="A" pin="21" pad="21"/>
<connect gate="A" pin="22" pad="22"/>
<connect gate="A" pin="23" pad="23"/>
<connect gate="A" pin="24" pad="24"/>
<connect gate="A" pin="25" pad="25"/>
<connect gate="A" pin="26" pad="26"/>
<connect gate="A" pin="27" pad="27"/>
<connect gate="A" pin="28" pad="28"/>
<connect gate="A" pin="29" pad="29"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="30" pad="30"/>
<connect gate="A" pin="31" pad="31"/>
<connect gate="A" pin="32" pad="32"/>
<connect gate="A" pin="33" pad="33"/>
<connect gate="A" pin="34" pad="34"/>
<connect gate="A" pin="35" pad="35"/>
<connect gate="A" pin="36" pad="36"/>
<connect gate="A" pin="37" pad="37"/>
<connect gate="A" pin="38" pad="38"/>
<connect gate="A" pin="39" pad="39"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="40" pad="40"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
<connect gate="A" pin="8" pad="8"/>
<connect gate="A" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="wirepad">
<description>&lt;b&gt;Single Pads&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="1,6/0,9">
<description>&lt;b&gt;THROUGH-HOLE PAD&lt;/b&gt;</description>
<wire x1="-0.508" y1="0.762" x2="-0.762" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="0.762" x2="-0.762" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="-0.508" x2="-0.762" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="-0.762" x2="-0.508" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="0.508" y1="-0.762" x2="0.762" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="0.762" y1="-0.762" x2="0.762" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="0.762" y1="0.508" x2="0.762" y2="0.762" width="0.1524" layer="21"/>
<wire x1="0.762" y1="0.762" x2="0.508" y2="0.762" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="0.635" width="0.1524" layer="51"/>
<pad name="1" x="0" y="0" drill="0.9144" diameter="1.6002" shape="octagon"/>
<text x="-0.762" y="1.016" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="0.6" size="0.0254" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="PAD">
<wire x1="-1.016" y1="1.016" x2="1.016" y2="-1.016" width="0.254" layer="94"/>
<wire x1="-1.016" y1="-1.016" x2="1.016" y2="1.016" width="0.254" layer="94"/>
<text x="-1.143" y="1.8542" size="1.778" layer="95">&gt;NAME</text>
<text x="-1.143" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="P" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="1,6/0,9" prefix="PAD" uservalue="yes">
<description>&lt;b&gt;THROUGH-HOLE PAD&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="PAD" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1,6/0,9">
<connects>
<connect gate="1" pin="P" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="VCC">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="VCC" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VCC" prefix="P+">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="VCC" symbol="VCC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-molex">
<description>&lt;b&gt;Molex Connectors&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="53047-03">
<description>&lt;b&gt;CONNECTOR&lt;/b&gt;&lt;p&gt;
wire to board 1.25 mm (.049 inch) pitch header&lt;p&gt;
straight&lt;br&gt;
Sourcxe: http://www.molex.com/pdm_docs/sd/530470210_sd.pdf</description>
<wire x1="-2.65" y1="-1.5" x2="2.65" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="2.65" y1="-1.5" x2="2.65" y2="1.5" width="0.2032" layer="21"/>
<wire x1="2.65" y1="1.5" x2="-2.65" y2="1.5" width="0.2032" layer="21"/>
<wire x1="-2.65" y1="1.5" x2="-2.65" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="-2.625" y1="-0.25" x2="-2.25" y2="-0.25" width="0.0508" layer="21"/>
<wire x1="-2.25" y1="-0.25" x2="-2.25" y2="-1.25" width="0.0508" layer="21"/>
<wire x1="2.25" y1="-0.25" x2="2.625" y2="-0.25" width="0.0508" layer="21"/>
<wire x1="2.25" y1="-0.25" x2="2.25" y2="-1.25" width="0.0508" layer="21"/>
<wire x1="-2.625" y1="0.375" x2="-2.25" y2="0.375" width="0.0508" layer="21"/>
<wire x1="2.25" y1="0.375" x2="2.625" y2="0.375" width="0.0508" layer="21"/>
<wire x1="-2.25" y1="0.375" x2="-2.25" y2="1.125" width="0.0508" layer="21"/>
<wire x1="-2.25" y1="1.125" x2="2.25" y2="1.125" width="0.0508" layer="21"/>
<wire x1="2.25" y1="1.125" x2="2.25" y2="0.375" width="0.0508" layer="21"/>
<wire x1="2.25" y1="1.125" x2="2.5" y2="1.375" width="0.0508" layer="21"/>
<wire x1="-2.25" y1="1.125" x2="-2.5" y2="1.375" width="0.0508" layer="21"/>
<pad name="1" x="1.25" y="0.45" drill="0.5" diameter="0.6984" shape="long" rot="R90"/>
<pad name="2" x="0" y="0.45" drill="0.5" diameter="0.6984" shape="long" rot="R90"/>
<pad name="3" x="-1.25" y="0.45" drill="0.5" diameter="0.6984" shape="long" rot="R90"/>
<text x="-2.5" y="1.75" size="1.27" layer="25">&gt;NAME</text>
<text x="-2" y="-3.25" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.625" y1="-1.5" x2="2.625" y2="-1.125" layer="21"/>
</package>
<package name="53048-03">
<description>&lt;b&gt;CONNECTOR&lt;/b&gt;&lt;p&gt;
wire to board 1.25 mm (.049 inch) pitch header&lt;p&gt;
right angle</description>
<wire x1="-2.65" y1="-2.25" x2="-2.125" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="2.125" y1="-2.25" x2="2.65" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="2.65" y1="-2.25" x2="2.65" y2="3.125" width="0.2032" layer="21"/>
<wire x1="2.65" y1="3.125" x2="2.5" y2="3.125" width="0.2032" layer="21"/>
<wire x1="2.5" y1="3.125" x2="-2.5" y2="3.125" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="3.125" x2="-2.65" y2="3.125" width="0.2032" layer="21"/>
<wire x1="-2.65" y1="3.125" x2="-2.65" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-2.625" y1="1.5" x2="-2.125" y2="1.5" width="0.0508" layer="21"/>
<wire x1="-2.125" y1="1.5" x2="-1.875" y2="1.5" width="0.0508" layer="21"/>
<wire x1="-1.875" y1="1.5" x2="-1.875" y2="0.625" width="0.0508" layer="21"/>
<wire x1="-1.875" y1="0.625" x2="1.875" y2="0.625" width="0.0508" layer="21"/>
<wire x1="1.875" y1="1.5" x2="2.125" y2="1.5" width="0.0508" layer="21"/>
<wire x1="2.125" y1="1.5" x2="2.625" y2="1.5" width="0.0508" layer="21"/>
<wire x1="1.875" y1="1.5" x2="1.875" y2="0.625" width="0.0508" layer="21"/>
<wire x1="-2.125" y1="-1" x2="2.125" y2="-1" width="0.2032" layer="51"/>
<wire x1="-1.5" y1="-1.5" x2="-1.625" y2="-1.625" width="0.2032" layer="51"/>
<wire x1="-0.25" y1="-1.5" x2="-0.375" y2="-1.625" width="0.2032" layer="51"/>
<wire x1="-0.375" y1="-1.625" x2="-0.875" y2="-1.625" width="0.2032" layer="51"/>
<wire x1="-1" y1="-1.5" x2="-0.875" y2="-1.625" width="0.2032" layer="51"/>
<wire x1="1" y1="-1.5" x2="0.875" y2="-1.625" width="0.2032" layer="51"/>
<wire x1="0.875" y1="-1.625" x2="0.375" y2="-1.625" width="0.2032" layer="51"/>
<wire x1="0.25" y1="-1.5" x2="0.375" y2="-1.625" width="0.2032" layer="51"/>
<wire x1="2.125" y1="-1.625" x2="1.625" y2="-1.625" width="0.2032" layer="51"/>
<wire x1="1.5" y1="-1.5" x2="1.625" y2="-1.625" width="0.2032" layer="51"/>
<wire x1="-1.625" y1="-1.625" x2="-2.125" y2="-1.625" width="0.2032" layer="51"/>
<wire x1="-2.625" y1="-1" x2="-2.125" y2="-1" width="0.2032" layer="21"/>
<wire x1="-2.125" y1="-1" x2="-2.125" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="2.125" y1="-1" x2="2.625" y2="-1" width="0.2032" layer="21"/>
<wire x1="2.125" y1="-1" x2="2.125" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-2.125" y1="1.5" x2="-2.125" y2="2.75" width="0.0508" layer="21"/>
<wire x1="-2.125" y1="2.75" x2="2.125" y2="2.75" width="0.0508" layer="21"/>
<wire x1="2.125" y1="2.75" x2="2.125" y2="1.5" width="0.0508" layer="21"/>
<wire x1="-2.125" y1="2.75" x2="-2.5" y2="3.125" width="0.0508" layer="21"/>
<wire x1="2.125" y1="2.75" x2="2.5" y2="3.125" width="0.0508" layer="21"/>
<wire x1="-1.375" y1="1.5" x2="-1.25" y2="2" width="0.2032" layer="21"/>
<wire x1="-1.25" y1="2" x2="-1.125" y2="1.5" width="0.2032" layer="21"/>
<wire x1="-0.125" y1="1.5" x2="0" y2="2" width="0.2032" layer="21"/>
<wire x1="0" y1="2" x2="0.125" y2="1.5" width="0.2032" layer="21"/>
<wire x1="1.125" y1="1.5" x2="1.25" y2="2" width="0.2032" layer="21"/>
<wire x1="1.25" y1="2" x2="1.375" y2="1.5" width="0.2032" layer="21"/>
<pad name="1" x="1.25" y="-1.25" drill="0.5" diameter="0.6984" shape="long" rot="R90"/>
<pad name="2" x="0" y="-1.25" drill="0.5" diameter="0.6984" shape="long" rot="R90"/>
<pad name="3" x="-1.25" y="-1.25" drill="0.5" diameter="0.6984" shape="long" rot="R90"/>
<text x="-2.5" y="3.375" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.625" y="-3.875" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.5" y1="-1.5" x2="-1" y2="-1" layer="51"/>
<rectangle x1="-0.25" y1="-1.5" x2="0.25" y2="-1" layer="51"/>
<rectangle x1="1" y1="-1.5" x2="1.5" y2="-1" layer="51"/>
<rectangle x1="-1.5" y1="0.625" x2="-1" y2="1.5" layer="21"/>
<rectangle x1="-0.25" y1="0.625" x2="0.25" y2="1.5" layer="21"/>
<rectangle x1="1" y1="0.625" x2="1.5" y2="1.5" layer="21"/>
</package>
<package name="53261-03">
<description>&lt;b&gt;CONNECTOR&lt;/b&gt;&lt;p&gt;
wire to board 1.25 mm (.049 inch) pitch header&lt;p&gt;
SMT&lt;p&gt;
right angle</description>
<wire x1="-2.65" y1="-1.375" x2="-2.125" y2="-1.375" width="0.2032" layer="21"/>
<wire x1="-2.125" y1="-1.375" x2="2.125" y2="-1.375" width="0.2032" layer="21"/>
<wire x1="2.125" y1="-1.375" x2="2.65" y2="-1.375" width="0.2032" layer="21"/>
<wire x1="2.65" y1="-1.375" x2="2.65" y2="2.625" width="0.2032" layer="21"/>
<wire x1="2.65" y1="2.625" x2="2.5" y2="2.625" width="0.2032" layer="21"/>
<wire x1="2.5" y1="2.625" x2="-2.5" y2="2.625" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="2.625" x2="-2.65" y2="2.625" width="0.2032" layer="21"/>
<wire x1="-2.65" y1="2.625" x2="-2.65" y2="-1.375" width="0.2032" layer="21"/>
<wire x1="-2.625" y1="1.625" x2="-2.125" y2="1.625" width="0.0508" layer="21"/>
<wire x1="-2.125" y1="1.625" x2="-1.875" y2="1.625" width="0.0508" layer="21"/>
<wire x1="-1.875" y1="1.625" x2="-1.875" y2="1" width="0.0508" layer="21"/>
<wire x1="-1.875" y1="1" x2="1.875" y2="1" width="0.0508" layer="21"/>
<wire x1="1.875" y1="1.625" x2="2.125" y2="1.625" width="0.0508" layer="21"/>
<wire x1="2.125" y1="1.625" x2="2.625" y2="1.625" width="0.0508" layer="21"/>
<wire x1="1.875" y1="1.625" x2="1.875" y2="1" width="0.0508" layer="21"/>
<wire x1="-2.625" y1="-0.75" x2="-2.125" y2="-0.75" width="0.0508" layer="21"/>
<wire x1="-2.125" y1="-0.75" x2="-2.125" y2="-1.375" width="0.0508" layer="21"/>
<wire x1="2.125" y1="-0.75" x2="2.625" y2="-0.75" width="0.0508" layer="21"/>
<wire x1="2.125" y1="-0.75" x2="2.125" y2="-1.375" width="0.0508" layer="21"/>
<wire x1="-2.125" y1="1.625" x2="-2.125" y2="2.25" width="0.0508" layer="21"/>
<wire x1="-2.125" y1="2.25" x2="2.125" y2="2.25" width="0.0508" layer="21"/>
<wire x1="2.125" y1="2.25" x2="2.125" y2="1.625" width="0.0508" layer="21"/>
<wire x1="-2.125" y1="2.25" x2="-2.5" y2="2.625" width="0.0508" layer="21"/>
<wire x1="2.125" y1="2.25" x2="2.5" y2="2.625" width="0.0508" layer="21"/>
<wire x1="-1.5" y1="-1.25" x2="-1.5" y2="-0.75" width="0.0508" layer="21"/>
<wire x1="-1.5" y1="-0.75" x2="-1" y2="-0.75" width="0.0508" layer="21"/>
<wire x1="-1" y1="-0.75" x2="-1" y2="-1.25" width="0.0508" layer="21"/>
<wire x1="-0.25" y1="-1.25" x2="-0.25" y2="-0.75" width="0.0508" layer="21"/>
<wire x1="-0.25" y1="-0.75" x2="0.25" y2="-0.75" width="0.0508" layer="21"/>
<wire x1="0.25" y1="-0.75" x2="0.25" y2="-1.25" width="0.0508" layer="21"/>
<wire x1="1" y1="-1.25" x2="1" y2="-0.75" width="0.0508" layer="21"/>
<wire x1="1" y1="-0.75" x2="1.5" y2="-0.75" width="0.0508" layer="21"/>
<wire x1="1.5" y1="-0.75" x2="1.5" y2="-1.25" width="0.0508" layer="21"/>
<wire x1="-2.75" y1="2.25" x2="-4.625" y2="2.25" width="0.2032" layer="51"/>
<wire x1="-4.625" y1="2.25" x2="-4.625" y2="-0.75" width="0.2032" layer="51"/>
<wire x1="-4.625" y1="-0.75" x2="-2.75" y2="-0.75" width="0.2032" layer="51"/>
<wire x1="2.75" y1="-0.75" x2="4.625" y2="-0.75" width="0.2032" layer="51"/>
<wire x1="4.625" y1="-0.75" x2="4.625" y2="2.25" width="0.2032" layer="51"/>
<wire x1="4.625" y1="2.25" x2="2.75" y2="2.25" width="0.2032" layer="51"/>
<smd name="1" x="1.25" y="-2.5" dx="0.8" dy="2" layer="1"/>
<smd name="2" x="0" y="-2.5" dx="0.8" dy="2" layer="1"/>
<smd name="3" x="-1.25" y="-2.5" dx="0.8" dy="2" layer="1"/>
<smd name="S1" x="3.75" y="0.625" dx="2.1" dy="3" layer="1"/>
<smd name="S2" x="-3.75" y="0.625" dx="2.1" dy="3" layer="1"/>
<text x="-2.5" y="2.875" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.25" y="-3" size="1.27" layer="27">&gt;VALUE</text>
<text x="3.125" y="-0.25" size="1.9304" layer="51">1</text>
<rectangle x1="-1.5" y1="1" x2="-1" y2="1.875" layer="21"/>
<rectangle x1="-0.25" y1="1" x2="0.25" y2="1.875" layer="21"/>
<rectangle x1="1" y1="1" x2="1.5" y2="1.875" layer="21"/>
</package>
<package name="53398-03">
<description>&lt;b&gt;CONNECTOR&lt;/b&gt;&lt;p&gt;
wire to board 1.25 mm (.049 inch) pitch header&lt;p&gt;
SMT&lt;p&gt;
straight</description>
<wire x1="-2.65" y1="-1.375" x2="-1.875" y2="-1.375" width="0.2032" layer="21"/>
<wire x1="-1.875" y1="-1.375" x2="1.875" y2="-1.375" width="0.0508" layer="21"/>
<wire x1="1.875" y1="-1.375" x2="2.65" y2="-1.375" width="0.2032" layer="21"/>
<wire x1="2.65" y1="-1.375" x2="2.65" y2="2.125" width="0.2032" layer="21"/>
<wire x1="2.65" y1="2.125" x2="2.5" y2="2.125" width="0.2032" layer="21"/>
<wire x1="2.5" y1="2.125" x2="-2.5" y2="2.125" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="2.125" x2="-2.65" y2="2.125" width="0.2032" layer="21"/>
<wire x1="-2.65" y1="2.125" x2="-2.65" y2="-1.375" width="0.2032" layer="21"/>
<wire x1="-2.625" y1="1" x2="-2.125" y2="1" width="0.0508" layer="21"/>
<wire x1="2.125" y1="1" x2="2.625" y2="1" width="0.0508" layer="21"/>
<wire x1="-2.625" y1="-0.25" x2="-2.125" y2="-0.25" width="0.0508" layer="21"/>
<wire x1="-2.125" y1="-0.25" x2="-2.125" y2="-1" width="0.0508" layer="21"/>
<wire x1="2.125" y1="-0.25" x2="2.625" y2="-0.25" width="0.0508" layer="21"/>
<wire x1="2.125" y1="-0.25" x2="2.125" y2="-1" width="0.0508" layer="21"/>
<wire x1="-2.125" y1="1" x2="-2.125" y2="1.75" width="0.0508" layer="21"/>
<wire x1="-2.125" y1="1.75" x2="2.125" y2="1.75" width="0.0508" layer="21"/>
<wire x1="2.125" y1="1.75" x2="2.125" y2="1" width="0.0508" layer="21"/>
<wire x1="-2.125" y1="1.75" x2="-2.5" y2="2.125" width="0.0508" layer="21"/>
<wire x1="2.125" y1="1.75" x2="2.5" y2="2.125" width="0.0508" layer="21"/>
<wire x1="-2.75" y1="2.125" x2="-4.625" y2="2.125" width="0.2032" layer="51"/>
<wire x1="-4.625" y1="2.125" x2="-4.625" y2="-0.75" width="0.2032" layer="51"/>
<wire x1="-4.625" y1="-0.75" x2="-2.75" y2="-0.75" width="0.2032" layer="51"/>
<wire x1="2.75" y1="-0.75" x2="4.625" y2="-0.75" width="0.2032" layer="51"/>
<wire x1="4.625" y1="-0.75" x2="4.625" y2="2.125" width="0.2032" layer="51"/>
<wire x1="4.625" y1="2.125" x2="2.75" y2="2.125" width="0.2032" layer="51"/>
<wire x1="-2.625" y1="-1" x2="-2.125" y2="-1" width="0.0508" layer="21"/>
<wire x1="-2.125" y1="-1" x2="-1.875" y2="-1" width="0.0508" layer="21"/>
<wire x1="-1.875" y1="-1" x2="-1.875" y2="-1.375" width="0.0508" layer="21"/>
<wire x1="2.125" y1="-1" x2="2.625" y2="-1" width="0.0508" layer="21"/>
<wire x1="1.875" y1="-1" x2="2.125" y2="-1" width="0.0508" layer="21"/>
<wire x1="1.875" y1="-1" x2="1.875" y2="-1.375" width="0.0508" layer="21"/>
<smd name="1" x="1.25" y="-2.5" dx="0.8" dy="1.8" layer="1"/>
<smd name="2" x="0" y="-2.5" dx="0.8" dy="1.8" layer="1"/>
<smd name="3" x="-1.25" y="-2.5" dx="0.8" dy="1.8" layer="1"/>
<smd name="S1" x="3.75" y="0.625" dx="2.1" dy="3" layer="1"/>
<smd name="S2" x="-3.75" y="0.625" dx="2.1" dy="3" layer="1"/>
<text x="-2.375" y="2.375" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.125" y="-3.5" size="1.27" layer="27">&gt;VALUE</text>
<text x="3.125" y="-0.25" size="1.9304" layer="51">1</text>
<rectangle x1="-1.5" y1="0.375" x2="-1" y2="1" layer="21"/>
<rectangle x1="-0.25" y1="0.375" x2="0.25" y2="1" layer="21"/>
<rectangle x1="1" y1="0.375" x2="1.5" y2="1" layer="21"/>
</package>
<package name="53047-08">
<description>&lt;b&gt;CONNECTOR&lt;/b&gt;&lt;p&gt;
wire to board 1.25 mm (.049 inch) pitch header&lt;p&gt;
straight&lt;br&gt;
Sourcxe: http://www.molex.com/pdm_docs/sd/530470210_sd.pdf</description>
<wire x1="-5.775" y1="-1.5" x2="5.775" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="5.775" y1="-1.5" x2="5.775" y2="1.5" width="0.2032" layer="21"/>
<wire x1="5.775" y1="1.5" x2="-5.775" y2="1.5" width="0.2032" layer="21"/>
<wire x1="-5.775" y1="1.5" x2="-5.775" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="-5.75" y1="-0.25" x2="-5.375" y2="-0.25" width="0.0508" layer="21"/>
<wire x1="-5.375" y1="-0.25" x2="-5.375" y2="-1.25" width="0.0508" layer="21"/>
<wire x1="5.375" y1="-0.25" x2="5.75" y2="-0.25" width="0.0508" layer="21"/>
<wire x1="5.375" y1="-0.25" x2="5.375" y2="-1.25" width="0.0508" layer="21"/>
<wire x1="-5.75" y1="0.375" x2="-5.375" y2="0.375" width="0.0508" layer="21"/>
<wire x1="5.375" y1="0.375" x2="5.75" y2="0.375" width="0.0508" layer="21"/>
<wire x1="-5.375" y1="0.375" x2="-5.375" y2="1.125" width="0.0508" layer="21"/>
<wire x1="-5.375" y1="1.125" x2="5.375" y2="1.125" width="0.0508" layer="21"/>
<wire x1="5.375" y1="1.125" x2="5.375" y2="0.375" width="0.0508" layer="21"/>
<wire x1="5.375" y1="1.125" x2="5.625" y2="1.375" width="0.0508" layer="21"/>
<wire x1="-5.375" y1="1.125" x2="-5.625" y2="1.375" width="0.0508" layer="21"/>
<pad name="1" x="4.375" y="0.45" drill="0.5" diameter="0.6984" shape="long" rot="R90"/>
<pad name="2" x="3.125" y="0.45" drill="0.5" diameter="0.6984" shape="long" rot="R90"/>
<pad name="3" x="1.875" y="0.45" drill="0.5" diameter="0.6984" shape="long" rot="R90"/>
<pad name="4" x="0.625" y="0.45" drill="0.5" diameter="0.6984" shape="long" rot="R90"/>
<pad name="5" x="-0.625" y="0.45" drill="0.5" diameter="0.6984" shape="long" rot="R90"/>
<pad name="6" x="-1.875" y="0.45" drill="0.5" diameter="0.6984" shape="long" rot="R90"/>
<pad name="7" x="-3.125" y="0.45" drill="0.5" diameter="0.6984" shape="long" rot="R90"/>
<pad name="8" x="-4.375" y="0.45" drill="0.5" diameter="0.6984" shape="long" rot="R90"/>
<text x="-5.625" y="1.75" size="1.27" layer="25">&gt;NAME</text>
<text x="1.125" y="1.75" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-5.75" y1="-1.5" x2="5.75" y2="-1.125" layer="21"/>
</package>
<package name="53048-08">
<description>&lt;b&gt;CONNECTOR&lt;/b&gt;&lt;p&gt;
wire to board 1.25 mm (.049 inch) pitch header&lt;p&gt;
right angle</description>
<wire x1="-5.775" y1="-2.25" x2="-5.25" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-2.25" x2="5.775" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="5.775" y1="-2.25" x2="5.775" y2="3.125" width="0.2032" layer="21"/>
<wire x1="5.775" y1="3.125" x2="5.625" y2="3.125" width="0.2032" layer="21"/>
<wire x1="5.625" y1="3.125" x2="-5.625" y2="3.125" width="0.2032" layer="21"/>
<wire x1="-5.625" y1="3.125" x2="-5.775" y2="3.125" width="0.2032" layer="21"/>
<wire x1="-5.775" y1="3.125" x2="-5.775" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-5.75" y1="1.5" x2="-5.25" y2="1.5" width="0.0508" layer="21"/>
<wire x1="-5.25" y1="1.5" x2="-5" y2="1.5" width="0.0508" layer="21"/>
<wire x1="-5" y1="1.5" x2="-5" y2="0.625" width="0.0508" layer="21"/>
<wire x1="-5" y1="0.625" x2="5" y2="0.625" width="0.0508" layer="21"/>
<wire x1="5" y1="1.5" x2="5.25" y2="1.5" width="0.0508" layer="21"/>
<wire x1="5.25" y1="1.5" x2="5.75" y2="1.5" width="0.0508" layer="21"/>
<wire x1="5" y1="1.5" x2="5" y2="0.625" width="0.0508" layer="21"/>
<wire x1="-5.25" y1="-1" x2="5.25" y2="-1" width="0.2032" layer="51"/>
<wire x1="-4.625" y1="-1.5" x2="-4.75" y2="-1.625" width="0.2032" layer="51"/>
<wire x1="-3.375" y1="-1.5" x2="-3.5" y2="-1.625" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="-1.625" x2="-4" y2="-1.625" width="0.2032" layer="51"/>
<wire x1="-4.125" y1="-1.5" x2="-4" y2="-1.625" width="0.2032" layer="51"/>
<wire x1="-2.125" y1="-1.5" x2="-2.25" y2="-1.625" width="0.2032" layer="51"/>
<wire x1="-2.25" y1="-1.625" x2="-2.75" y2="-1.625" width="0.2032" layer="51"/>
<wire x1="-2.875" y1="-1.5" x2="-2.75" y2="-1.625" width="0.2032" layer="51"/>
<wire x1="-0.875" y1="-1.5" x2="-1" y2="-1.625" width="0.2032" layer="51"/>
<wire x1="-1" y1="-1.625" x2="-1.5" y2="-1.625" width="0.2032" layer="51"/>
<wire x1="-1.625" y1="-1.5" x2="-1.5" y2="-1.625" width="0.2032" layer="51"/>
<wire x1="0.375" y1="-1.5" x2="0.25" y2="-1.625" width="0.2032" layer="51"/>
<wire x1="0.25" y1="-1.625" x2="-0.25" y2="-1.625" width="0.2032" layer="51"/>
<wire x1="-0.375" y1="-1.5" x2="-0.25" y2="-1.625" width="0.2032" layer="51"/>
<wire x1="1.625" y1="-1.5" x2="1.5" y2="-1.625" width="0.2032" layer="51"/>
<wire x1="1.5" y1="-1.625" x2="1" y2="-1.625" width="0.2032" layer="51"/>
<wire x1="0.875" y1="-1.5" x2="1" y2="-1.625" width="0.2032" layer="51"/>
<wire x1="2.875" y1="-1.5" x2="2.75" y2="-1.625" width="0.2032" layer="51"/>
<wire x1="2.75" y1="-1.625" x2="2.25" y2="-1.625" width="0.2032" layer="51"/>
<wire x1="2.125" y1="-1.5" x2="2.25" y2="-1.625" width="0.2032" layer="51"/>
<wire x1="4.125" y1="-1.5" x2="4" y2="-1.625" width="0.2032" layer="51"/>
<wire x1="4" y1="-1.625" x2="3.5" y2="-1.625" width="0.2032" layer="51"/>
<wire x1="3.375" y1="-1.5" x2="3.5" y2="-1.625" width="0.2032" layer="51"/>
<wire x1="5.25" y1="-1.625" x2="4.75" y2="-1.625" width="0.2032" layer="51"/>
<wire x1="4.625" y1="-1.5" x2="4.75" y2="-1.625" width="0.2032" layer="51"/>
<wire x1="-4.75" y1="-1.625" x2="-5.25" y2="-1.625" width="0.2032" layer="51"/>
<wire x1="-5.75" y1="-1" x2="-5.25" y2="-1" width="0.2032" layer="21"/>
<wire x1="-5.25" y1="-1" x2="-5.25" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-1" x2="5.75" y2="-1" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-1" x2="5.25" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-5.25" y1="1.5" x2="-5.25" y2="2.75" width="0.0508" layer="21"/>
<wire x1="-5.25" y1="2.75" x2="5.25" y2="2.75" width="0.0508" layer="21"/>
<wire x1="5.25" y1="2.75" x2="5.25" y2="1.5" width="0.0508" layer="21"/>
<wire x1="-5.25" y1="2.75" x2="-5.625" y2="3.125" width="0.0508" layer="21"/>
<wire x1="5.25" y1="2.75" x2="5.625" y2="3.125" width="0.0508" layer="21"/>
<wire x1="-4.5" y1="1.5" x2="-4.375" y2="2" width="0.2032" layer="21"/>
<wire x1="-4.375" y1="2" x2="-4.25" y2="1.5" width="0.2032" layer="21"/>
<wire x1="-3.25" y1="1.5" x2="-3.125" y2="2" width="0.2032" layer="21"/>
<wire x1="-3.125" y1="2" x2="-3" y2="1.5" width="0.2032" layer="21"/>
<wire x1="-2" y1="1.5" x2="-1.875" y2="2" width="0.2032" layer="21"/>
<wire x1="-1.875" y1="2" x2="-1.75" y2="1.5" width="0.2032" layer="21"/>
<wire x1="-0.75" y1="1.5" x2="-0.625" y2="2" width="0.2032" layer="21"/>
<wire x1="-0.625" y1="2" x2="-0.5" y2="1.5" width="0.2032" layer="21"/>
<wire x1="0.5" y1="1.5" x2="0.625" y2="2" width="0.2032" layer="21"/>
<wire x1="0.625" y1="2" x2="0.75" y2="1.5" width="0.2032" layer="21"/>
<wire x1="1.75" y1="1.5" x2="1.875" y2="2" width="0.2032" layer="21"/>
<wire x1="1.875" y1="2" x2="2" y2="1.5" width="0.2032" layer="21"/>
<wire x1="3" y1="1.5" x2="3.125" y2="2" width="0.2032" layer="21"/>
<wire x1="3.125" y1="2" x2="3.25" y2="1.5" width="0.2032" layer="21"/>
<wire x1="4.25" y1="1.5" x2="4.375" y2="2" width="0.2032" layer="21"/>
<wire x1="4.375" y1="2" x2="4.5" y2="1.5" width="0.2032" layer="21"/>
<pad name="1" x="4.375" y="-1.25" drill="0.5" diameter="0.6984" shape="long" rot="R90"/>
<pad name="2" x="3.125" y="-1.25" drill="0.5" diameter="0.6984" shape="long" rot="R90"/>
<pad name="3" x="1.875" y="-1.25" drill="0.5" diameter="0.6984" shape="long" rot="R90"/>
<pad name="4" x="0.625" y="-1.25" drill="0.5" diameter="0.6984" shape="long" rot="R90"/>
<pad name="5" x="-0.625" y="-1.25" drill="0.5" diameter="0.6984" shape="long" rot="R90"/>
<pad name="6" x="-1.875" y="-1.25" drill="0.5" diameter="0.6984" shape="long" rot="R90"/>
<pad name="7" x="-3.125" y="-1.25" drill="0.5" diameter="0.6984" shape="long" rot="R90"/>
<pad name="8" x="-4.375" y="-1.25" drill="0.5" diameter="0.6984" shape="long" rot="R90"/>
<text x="-4.375" y="3.375" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.375" y="-3.75" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-4.625" y1="-1.5" x2="-4.125" y2="-1" layer="51"/>
<rectangle x1="-3.375" y1="-1.5" x2="-2.875" y2="-1" layer="51"/>
<rectangle x1="-2.125" y1="-1.5" x2="-1.625" y2="-1" layer="51"/>
<rectangle x1="-0.875" y1="-1.5" x2="-0.375" y2="-1" layer="51"/>
<rectangle x1="0.375" y1="-1.5" x2="0.875" y2="-1" layer="51"/>
<rectangle x1="1.625" y1="-1.5" x2="2.125" y2="-1" layer="51"/>
<rectangle x1="2.875" y1="-1.5" x2="3.375" y2="-1" layer="51"/>
<rectangle x1="4.125" y1="-1.5" x2="4.625" y2="-1" layer="51"/>
<rectangle x1="-4.625" y1="0.625" x2="-4.125" y2="1.5" layer="21"/>
<rectangle x1="-3.375" y1="0.625" x2="-2.875" y2="1.5" layer="21"/>
<rectangle x1="-2.125" y1="0.625" x2="-1.625" y2="1.5" layer="21"/>
<rectangle x1="-0.875" y1="0.625" x2="-0.375" y2="1.5" layer="21"/>
<rectangle x1="0.375" y1="0.625" x2="0.875" y2="1.5" layer="21"/>
<rectangle x1="1.625" y1="0.625" x2="2.125" y2="1.5" layer="21"/>
<rectangle x1="2.875" y1="0.625" x2="3.375" y2="1.5" layer="21"/>
<rectangle x1="4.125" y1="0.625" x2="4.625" y2="1.5" layer="21"/>
</package>
<package name="53261-08">
<description>&lt;b&gt;CONNECTOR&lt;/b&gt;&lt;p&gt;
wire to board 1.25 mm (.049 inch) pitch header&lt;p&gt;
SMT&lt;p&gt;
right angle</description>
<wire x1="-5.775" y1="-1.375" x2="-5.25" y2="-1.375" width="0.2032" layer="21"/>
<wire x1="-5.25" y1="-1.375" x2="5.25" y2="-1.375" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-1.375" x2="5.775" y2="-1.375" width="0.2032" layer="21"/>
<wire x1="5.775" y1="-1.375" x2="5.775" y2="2.625" width="0.2032" layer="21"/>
<wire x1="5.775" y1="2.625" x2="5.625" y2="2.625" width="0.2032" layer="21"/>
<wire x1="5.625" y1="2.625" x2="-5.625" y2="2.625" width="0.2032" layer="21"/>
<wire x1="-5.625" y1="2.625" x2="-5.775" y2="2.625" width="0.2032" layer="21"/>
<wire x1="-5.775" y1="2.625" x2="-5.775" y2="-1.375" width="0.2032" layer="21"/>
<wire x1="-5.75" y1="1.625" x2="-5.25" y2="1.625" width="0.0508" layer="21"/>
<wire x1="-5.25" y1="1.625" x2="-5" y2="1.625" width="0.0508" layer="21"/>
<wire x1="-5" y1="1.625" x2="-5" y2="1" width="0.0508" layer="21"/>
<wire x1="-5" y1="1" x2="5" y2="1" width="0.0508" layer="21"/>
<wire x1="5" y1="1.625" x2="5.25" y2="1.625" width="0.0508" layer="21"/>
<wire x1="5.25" y1="1.625" x2="5.75" y2="1.625" width="0.0508" layer="21"/>
<wire x1="5" y1="1.625" x2="5" y2="1" width="0.0508" layer="21"/>
<wire x1="-5.75" y1="-0.75" x2="-5.25" y2="-0.75" width="0.0508" layer="21"/>
<wire x1="-5.25" y1="-0.75" x2="-5.25" y2="-1.375" width="0.0508" layer="21"/>
<wire x1="5.25" y1="-0.75" x2="5.75" y2="-0.75" width="0.0508" layer="21"/>
<wire x1="5.25" y1="-0.75" x2="5.25" y2="-1.375" width="0.0508" layer="21"/>
<wire x1="-5.25" y1="1.625" x2="-5.25" y2="2.25" width="0.0508" layer="21"/>
<wire x1="-5.25" y1="2.25" x2="5.25" y2="2.25" width="0.0508" layer="21"/>
<wire x1="5.25" y1="2.25" x2="5.25" y2="1.625" width="0.0508" layer="21"/>
<wire x1="-5.25" y1="2.25" x2="-5.625" y2="2.625" width="0.0508" layer="21"/>
<wire x1="5.25" y1="2.25" x2="5.625" y2="2.625" width="0.0508" layer="21"/>
<wire x1="-4.625" y1="-1.25" x2="-4.625" y2="-0.75" width="0.0508" layer="21"/>
<wire x1="-4.625" y1="-0.75" x2="-4.125" y2="-0.75" width="0.0508" layer="21"/>
<wire x1="-4.125" y1="-0.75" x2="-4.125" y2="-1.25" width="0.0508" layer="21"/>
<wire x1="-3.375" y1="-1.25" x2="-3.375" y2="-0.75" width="0.0508" layer="21"/>
<wire x1="-3.375" y1="-0.75" x2="-2.875" y2="-0.75" width="0.0508" layer="21"/>
<wire x1="-2.875" y1="-0.75" x2="-2.875" y2="-1.25" width="0.0508" layer="21"/>
<wire x1="-2.125" y1="-1.25" x2="-2.125" y2="-0.75" width="0.0508" layer="21"/>
<wire x1="-2.125" y1="-0.75" x2="-1.625" y2="-0.75" width="0.0508" layer="21"/>
<wire x1="-1.625" y1="-0.75" x2="-1.625" y2="-1.25" width="0.0508" layer="21"/>
<wire x1="-0.875" y1="-1.25" x2="-0.875" y2="-0.75" width="0.0508" layer="21"/>
<wire x1="-0.875" y1="-0.75" x2="-0.375" y2="-0.75" width="0.0508" layer="21"/>
<wire x1="-0.375" y1="-0.75" x2="-0.375" y2="-1.25" width="0.0508" layer="21"/>
<wire x1="0.375" y1="-1.25" x2="0.375" y2="-0.75" width="0.0508" layer="21"/>
<wire x1="0.375" y1="-0.75" x2="0.875" y2="-0.75" width="0.0508" layer="21"/>
<wire x1="0.875" y1="-0.75" x2="0.875" y2="-1.25" width="0.0508" layer="21"/>
<wire x1="1.625" y1="-1.25" x2="1.625" y2="-0.75" width="0.0508" layer="21"/>
<wire x1="1.625" y1="-0.75" x2="2.125" y2="-0.75" width="0.0508" layer="21"/>
<wire x1="2.125" y1="-0.75" x2="2.125" y2="-1.25" width="0.0508" layer="21"/>
<wire x1="2.875" y1="-1.25" x2="2.875" y2="-0.75" width="0.0508" layer="21"/>
<wire x1="2.875" y1="-0.75" x2="3.375" y2="-0.75" width="0.0508" layer="21"/>
<wire x1="3.375" y1="-0.75" x2="3.375" y2="-1.25" width="0.0508" layer="21"/>
<wire x1="4.125" y1="-1.25" x2="4.125" y2="-0.75" width="0.0508" layer="21"/>
<wire x1="4.125" y1="-0.75" x2="4.625" y2="-0.75" width="0.0508" layer="21"/>
<wire x1="4.625" y1="-0.75" x2="4.625" y2="-1.25" width="0.0508" layer="21"/>
<wire x1="-5.875" y1="2.25" x2="-7.75" y2="2.25" width="0.2032" layer="51"/>
<wire x1="-7.75" y1="2.25" x2="-7.75" y2="-0.75" width="0.2032" layer="51"/>
<wire x1="-7.75" y1="-0.75" x2="-5.875" y2="-0.75" width="0.2032" layer="51"/>
<wire x1="5.875" y1="-0.75" x2="7.75" y2="-0.75" width="0.2032" layer="51"/>
<wire x1="7.75" y1="-0.75" x2="7.75" y2="2.25" width="0.2032" layer="51"/>
<wire x1="7.75" y1="2.25" x2="5.875" y2="2.25" width="0.2032" layer="51"/>
<smd name="1" x="4.375" y="-2.5" dx="0.8" dy="2" layer="1"/>
<smd name="2" x="3.125" y="-2.5" dx="0.8" dy="2" layer="1"/>
<smd name="3" x="1.875" y="-2.5" dx="0.8" dy="2" layer="1"/>
<smd name="4" x="0.625" y="-2.5" dx="0.8" dy="2" layer="1"/>
<smd name="5" x="-0.625" y="-2.5" dx="0.8" dy="2" layer="1"/>
<smd name="6" x="-1.875" y="-2.5" dx="0.8" dy="2" layer="1"/>
<smd name="7" x="-3.125" y="-2.5" dx="0.8" dy="2" layer="1"/>
<smd name="8" x="-4.375" y="-2.5" dx="0.8" dy="2" layer="1"/>
<smd name="S1" x="6.875" y="0.625" dx="2.1" dy="3" layer="1"/>
<smd name="S2" x="-6.875" y="0.625" dx="2.1" dy="3" layer="1"/>
<text x="-5.625" y="2.875" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.25" y="-0.5" size="1.27" layer="27">&gt;VALUE</text>
<text x="6.25" y="-0.25" size="1.9304" layer="51">1</text>
<rectangle x1="-4.625" y1="1" x2="-4.125" y2="1.875" layer="21"/>
<rectangle x1="-3.375" y1="1" x2="-2.875" y2="1.875" layer="21"/>
<rectangle x1="-2.125" y1="1" x2="-1.625" y2="1.875" layer="21"/>
<rectangle x1="-0.875" y1="1" x2="-0.375" y2="1.875" layer="21"/>
<rectangle x1="0.375" y1="1" x2="0.875" y2="1.875" layer="21"/>
<rectangle x1="1.625" y1="1" x2="2.125" y2="1.875" layer="21"/>
<rectangle x1="2.875" y1="1" x2="3.375" y2="1.875" layer="21"/>
<rectangle x1="4.125" y1="1" x2="4.625" y2="1.875" layer="21"/>
</package>
<package name="53398-08">
<description>&lt;b&gt;CONNECTOR&lt;/b&gt;&lt;p&gt;
wire to board 1.25 mm (.049 inch) pitch header&lt;p&gt;
SMT&lt;p&gt;
straight</description>
<wire x1="-5.775" y1="-1.375" x2="-5" y2="-1.375" width="0.2032" layer="21"/>
<wire x1="-5" y1="-1.375" x2="5" y2="-1.375" width="0.0508" layer="21"/>
<wire x1="5" y1="-1.375" x2="5.775" y2="-1.375" width="0.2032" layer="21"/>
<wire x1="5.775" y1="-1.375" x2="5.775" y2="2.125" width="0.2032" layer="21"/>
<wire x1="5.775" y1="2.125" x2="5.625" y2="2.125" width="0.2032" layer="21"/>
<wire x1="5.625" y1="2.125" x2="-5.625" y2="2.125" width="0.2032" layer="21"/>
<wire x1="-5.625" y1="2.125" x2="-5.775" y2="2.125" width="0.2032" layer="21"/>
<wire x1="-5.775" y1="2.125" x2="-5.775" y2="-1.375" width="0.2032" layer="21"/>
<wire x1="-5.75" y1="1" x2="-5.25" y2="1" width="0.0508" layer="21"/>
<wire x1="5.25" y1="1" x2="5.75" y2="1" width="0.0508" layer="21"/>
<wire x1="-5.75" y1="-0.25" x2="-5.25" y2="-0.25" width="0.0508" layer="21"/>
<wire x1="-5.25" y1="-0.25" x2="-5.25" y2="-1" width="0.0508" layer="21"/>
<wire x1="5.25" y1="-0.25" x2="5.75" y2="-0.25" width="0.0508" layer="21"/>
<wire x1="5.25" y1="-0.25" x2="5.25" y2="-1" width="0.0508" layer="21"/>
<wire x1="-5.25" y1="1" x2="-5.25" y2="1.75" width="0.0508" layer="21"/>
<wire x1="-5.25" y1="1.75" x2="5.25" y2="1.75" width="0.0508" layer="21"/>
<wire x1="5.25" y1="1.75" x2="5.25" y2="1" width="0.0508" layer="21"/>
<wire x1="-5.25" y1="1.75" x2="-5.625" y2="2.125" width="0.0508" layer="21"/>
<wire x1="5.25" y1="1.75" x2="5.625" y2="2.125" width="0.0508" layer="21"/>
<wire x1="-5.875" y1="2.125" x2="-7.75" y2="2.125" width="0.2032" layer="51"/>
<wire x1="-7.75" y1="2.125" x2="-7.75" y2="-0.75" width="0.2032" layer="51"/>
<wire x1="-7.75" y1="-0.75" x2="-5.875" y2="-0.75" width="0.2032" layer="51"/>
<wire x1="5.875" y1="-0.75" x2="7.75" y2="-0.75" width="0.2032" layer="51"/>
<wire x1="7.75" y1="-0.75" x2="7.75" y2="2.125" width="0.2032" layer="51"/>
<wire x1="7.75" y1="2.125" x2="5.875" y2="2.125" width="0.2032" layer="51"/>
<wire x1="-5.75" y1="-1" x2="-5.25" y2="-1" width="0.0508" layer="21"/>
<wire x1="-5.25" y1="-1" x2="-5" y2="-1" width="0.0508" layer="21"/>
<wire x1="-5" y1="-1" x2="-5" y2="-1.375" width="0.0508" layer="21"/>
<wire x1="5.25" y1="-1" x2="5.75" y2="-1" width="0.0508" layer="21"/>
<wire x1="5" y1="-1" x2="5.25" y2="-1" width="0.0508" layer="21"/>
<wire x1="5" y1="-1" x2="5" y2="-1.375" width="0.0508" layer="21"/>
<smd name="1" x="4.375" y="-2.5" dx="0.8" dy="1.8" layer="1"/>
<smd name="2" x="3.125" y="-2.5" dx="0.8" dy="1.8" layer="1"/>
<smd name="3" x="1.875" y="-2.5" dx="0.8" dy="1.8" layer="1"/>
<smd name="4" x="0.625" y="-2.5" dx="0.8" dy="1.8" layer="1"/>
<smd name="5" x="-0.625" y="-2.5" dx="0.8" dy="1.8" layer="1"/>
<smd name="6" x="-1.875" y="-2.5" dx="0.8" dy="1.8" layer="1"/>
<smd name="7" x="-3.125" y="-2.5" dx="0.8" dy="1.8" layer="1"/>
<smd name="8" x="-4.375" y="-2.5" dx="0.8" dy="1.8" layer="1"/>
<smd name="S1" x="6.875" y="0.625" dx="2.1" dy="3" layer="1"/>
<smd name="S2" x="-6.875" y="0.625" dx="2.1" dy="3" layer="1"/>
<text x="-5.5" y="2.375" size="1.27" layer="25">&gt;NAME</text>
<text x="-5" y="-1" size="1.27" layer="27">&gt;VALUE</text>
<text x="6.25" y="-0.25" size="1.9304" layer="51">1</text>
<rectangle x1="-4.625" y1="0.375" x2="-4.125" y2="1" layer="21"/>
<rectangle x1="-3.375" y1="0.375" x2="-2.875" y2="1" layer="21"/>
<rectangle x1="-2.125" y1="0.375" x2="-1.625" y2="1" layer="21"/>
<rectangle x1="-0.875" y1="0.375" x2="-0.375" y2="1" layer="21"/>
<rectangle x1="0.375" y1="0.375" x2="0.875" y2="1" layer="21"/>
<rectangle x1="1.625" y1="0.375" x2="2.125" y2="1" layer="21"/>
<rectangle x1="2.875" y1="0.375" x2="3.375" y2="1" layer="21"/>
<rectangle x1="4.125" y1="0.375" x2="4.625" y2="1" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="MV">
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<text x="2.54" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<text x="-0.762" y="1.397" size="1.778" layer="96">&gt;VALUE</text>
<pin name="S" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
</symbol>
<symbol name="M">
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<text x="2.54" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<pin name="S" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="53?-03" prefix="X">
<description>&lt;b&gt;CONNECTOR&lt;/b&gt;&lt;p&gt;
wire to board 1.25 mm (.049 inch) pitch header</description>
<gates>
<gate name="-1" symbol="MV" x="0" y="2.54" addlevel="always" swaplevel="1"/>
<gate name="-2" symbol="M" x="0" y="0" addlevel="always" swaplevel="1"/>
<gate name="-3" symbol="M" x="0" y="-2.54" addlevel="always" swaplevel="1"/>
</gates>
<devices>
<device name="047" package="53047-03">
<connects>
<connect gate="-1" pin="S" pad="1"/>
<connect gate="-2" pin="S" pad="2"/>
<connect gate="-3" pin="S" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="530470310" constant="no"/>
<attribute name="OC_FARNELL" value="9732853" constant="no"/>
<attribute name="OC_NEWARK" value="14R5412" constant="no"/>
</technology>
</technologies>
</device>
<device name="048" package="53048-03">
<connects>
<connect gate="-1" pin="S" pad="1"/>
<connect gate="-2" pin="S" pad="2"/>
<connect gate="-3" pin="S" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="WALDOM/MOLEX" constant="no"/>
<attribute name="MPN" value="53048-0310" constant="no"/>
<attribute name="OC_FARNELL" value="9732918" constant="no"/>
<attribute name="OC_NEWARK" value="38C9908" constant="no"/>
</technology>
</technologies>
</device>
<device name="261" package="53261-03">
<connects>
<connect gate="-1" pin="S" pad="1"/>
<connect gate="-2" pin="S" pad="2"/>
<connect gate="-3" pin="S" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="MOLEX" constant="no"/>
<attribute name="MPN" value="53261-0390" constant="no"/>
<attribute name="OC_FARNELL" value="1125359" constant="no"/>
<attribute name="OC_NEWARK" value="57H5006" constant="no"/>
</technology>
</technologies>
</device>
<device name="398" package="53398-03">
<connects>
<connect gate="-1" pin="S" pad="1"/>
<connect gate="-2" pin="S" pad="2"/>
<connect gate="-3" pin="S" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="53?-08" prefix="X">
<description>&lt;b&gt;CONNECTOR&lt;/b&gt;&lt;p&gt;
wire to board 1.25 mm (.049 inch) pitch header</description>
<gates>
<gate name="-1" symbol="MV" x="0" y="7.62" addlevel="always" swaplevel="1"/>
<gate name="-2" symbol="M" x="0" y="5.08" addlevel="always" swaplevel="1"/>
<gate name="-3" symbol="M" x="0" y="2.54" addlevel="always" swaplevel="1"/>
<gate name="-4" symbol="M" x="0" y="0" addlevel="always" swaplevel="1"/>
<gate name="-5" symbol="M" x="0" y="-2.54" addlevel="always" swaplevel="1"/>
<gate name="-6" symbol="M" x="0" y="-5.08" addlevel="always" swaplevel="1"/>
<gate name="-7" symbol="M" x="0" y="-7.62" addlevel="always" swaplevel="1"/>
<gate name="-8" symbol="M" x="0" y="-10.16" addlevel="always" swaplevel="1"/>
</gates>
<devices>
<device name="047" package="53047-08">
<connects>
<connect gate="-1" pin="S" pad="1"/>
<connect gate="-2" pin="S" pad="2"/>
<connect gate="-3" pin="S" pad="3"/>
<connect gate="-4" pin="S" pad="4"/>
<connect gate="-5" pin="S" pad="5"/>
<connect gate="-6" pin="S" pad="6"/>
<connect gate="-7" pin="S" pad="7"/>
<connect gate="-8" pin="S" pad="8"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="53047-0810" constant="no"/>
<attribute name="OC_FARNELL" value="1012258" constant="no"/>
<attribute name="OC_NEWARK" value="98K9826" constant="no"/>
</technology>
</technologies>
</device>
<device name="048" package="53048-08">
<connects>
<connect gate="-1" pin="S" pad="1"/>
<connect gate="-2" pin="S" pad="2"/>
<connect gate="-3" pin="S" pad="3"/>
<connect gate="-4" pin="S" pad="4"/>
<connect gate="-5" pin="S" pad="5"/>
<connect gate="-6" pin="S" pad="6"/>
<connect gate="-7" pin="S" pad="7"/>
<connect gate="-8" pin="S" pad="8"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="WALDOM/MOLEX" constant="no"/>
<attribute name="MPN" value="53048-0810" constant="no"/>
<attribute name="OC_FARNELL" value="9733094" constant="no"/>
<attribute name="OC_NEWARK" value="38C9913" constant="no"/>
</technology>
</technologies>
</device>
<device name="261" package="53261-08">
<connects>
<connect gate="-1" pin="S" pad="1"/>
<connect gate="-2" pin="S" pad="2"/>
<connect gate="-3" pin="S" pad="3"/>
<connect gate="-4" pin="S" pad="4"/>
<connect gate="-5" pin="S" pad="5"/>
<connect gate="-6" pin="S" pad="6"/>
<connect gate="-7" pin="S" pad="7"/>
<connect gate="-8" pin="S" pad="8"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="1125376" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="398" package="53398-08">
<connects>
<connect gate="-1" pin="S" pad="1"/>
<connect gate="-2" pin="S" pad="2"/>
<connect gate="-3" pin="S" pad="3"/>
<connect gate="-4" pin="S" pad="4"/>
<connect gate="-5" pin="S" pad="5"/>
<connect gate="-6" pin="S" pad="6"/>
<connect gate="-7" pin="S" pad="7"/>
<connect gate="-8" pin="S" pad="8"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="1125371" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="JP3" library="pinhead" deviceset="PINHD-2X20" device=""/>
<part name="PCAMS_5V" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="PCAMS_GND" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="BP_RX" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="BP_TX" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="BNO_SDA" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="BNO_SCL" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="GND1" library="supply1" deviceset="GND" device=""/>
<part name="P+1" library="supply1" deviceset="VCC" device=""/>
<part name="P+2" library="supply1" deviceset="VCC" device=""/>
<part name="GND2" library="supply1" deviceset="GND" device=""/>
<part name="XSHUT_R" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="XSHUT_M" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="XSHUT_L" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="GND3" library="supply1" deviceset="GND" device=""/>
<part name="ENC11" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="ENC21" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="ENC12" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="ENC22" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="END31" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="END32" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="SDA" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="SCL" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="PCAMS_3.3V" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="PCAMS_BMON" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="PCAMS_X" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="BNO_3.3V" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="BNO_GND" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="P_GND" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="XSHUT_LEFT" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="XSHUT_RIGHT" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="RIGHT_TOF" library="con-molex" deviceset="53?-03" device="398"/>
<part name="MOC-SSIU" library="con-molex" deviceset="53?-08" device="398"/>
<part name="FRONT_TOF" library="con-molex" deviceset="53?-08" device="398"/>
<part name="P_RX" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="P_TX" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="BP_GND" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="BP_5V" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="BP_GND1" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="BP_GND2" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="BP_3.3V_" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="BP_3.3V_1" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="LEFT_TOF" library="con-molex" deviceset="53?-03" device="398"/>
<part name="JMP_TX" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="JMP_RX" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="JMP_TX1" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="JMP_RX1" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="VBAT" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="LED" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="OIN" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="OOUT" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="PWM1" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="PWM2" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="SS1" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="SCK1" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="MISO1" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="MOSI1" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="BMON" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="RST" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="PB1" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="PB5" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="PB4" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="PB3" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="PA15" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="PA12" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="PA11" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="MOSI2" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="MISO2" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="SCK2" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="SS2" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="PWM2_" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="PWM1_" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="SCL1" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="SDA1" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="PI_SDA" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="PI_SCL" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="RST1" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="5V_" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="GND_" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="PA1" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="PB3_" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="PB4_" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="GND_1" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="PCAMS_3.3V1" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="PCAMS_5V1" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="X" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="X1" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="X3" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="X4" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="X5" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="X6" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="X7" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="X8" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="X9" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="GND_2" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="X2" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="PB5_" library="wirepad" deviceset="1,6/0,9" device=""/>
<part name="PB1_" library="wirepad" deviceset="1,6/0,9" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="JP3" gate="A" x="-22.86" y="45.72"/>
<instance part="PCAMS_5V" gate="1" x="0" y="68.58" rot="R180"/>
<instance part="PCAMS_GND" gate="1" x="0" y="63.5" rot="R180"/>
<instance part="BP_RX" gate="1" x="66.04" y="53.34" smashed="yes" rot="R180">
<attribute name="NAME" x="62.103" y="54.0258" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="67.183" y="56.642" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="BP_TX" gate="1" x="66.04" y="55.88" smashed="yes" rot="R180">
<attribute name="NAME" x="62.103" y="56.5658" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="67.183" y="59.182" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="BNO_SDA" gate="1" x="-43.18" y="66.04" smashed="yes">
<attribute name="NAME" x="-57.023" y="65.3542" size="1.778" layer="95"/>
<attribute name="VALUE" x="-44.323" y="62.738" size="1.778" layer="96"/>
</instance>
<instance part="BNO_SCL" gate="1" x="-43.18" y="63.5" smashed="yes">
<attribute name="NAME" x="-57.023" y="62.8142" size="1.778" layer="95"/>
<attribute name="VALUE" x="-44.323" y="60.198" size="1.778" layer="96"/>
</instance>
<instance part="GND1" gate="1" x="-5.08" y="38.1"/>
<instance part="P+1" gate="VCC" x="-5.08" y="83.82"/>
<instance part="P+2" gate="VCC" x="10.16" y="30.48"/>
<instance part="GND2" gate="1" x="10.16" y="12.7"/>
<instance part="XSHUT_R" gate="1" x="66.04" y="68.58" smashed="yes" rot="R180">
<attribute name="NAME" x="62.103" y="69.2658" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="67.183" y="71.882" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="XSHUT_M" gate="1" x="66.04" y="66.04" smashed="yes" rot="R180">
<attribute name="NAME" x="62.103" y="66.7258" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="67.183" y="69.342" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="XSHUT_L" gate="1" x="66.04" y="63.5" smashed="yes" rot="R180">
<attribute name="NAME" x="62.103" y="64.1858" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="67.183" y="66.802" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND3" gate="1" x="111.76" y="58.42"/>
<instance part="ENC11" gate="1" x="81.28" y="30.48" smashed="yes">
<attribute name="NAME" x="82.677" y="29.7942" size="1.778" layer="95"/>
<attribute name="VALUE" x="80.137" y="27.178" size="1.778" layer="96"/>
</instance>
<instance part="ENC21" gate="1" x="66.04" y="30.48" smashed="yes" rot="R180">
<attribute name="NAME" x="62.103" y="31.1658" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="67.183" y="33.782" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="ENC12" gate="1" x="66.04" y="27.94" smashed="yes" rot="R180">
<attribute name="NAME" x="62.103" y="28.6258" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="67.183" y="31.242" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="ENC22" gate="1" x="81.28" y="33.02" smashed="yes">
<attribute name="NAME" x="82.677" y="32.3342" size="1.778" layer="95"/>
<attribute name="VALUE" x="80.137" y="29.718" size="1.778" layer="96"/>
</instance>
<instance part="END31" gate="1" x="81.28" y="58.42" smashed="yes">
<attribute name="NAME" x="82.677" y="57.7342" size="1.778" layer="95"/>
<attribute name="VALUE" x="80.137" y="55.118" size="1.778" layer="96"/>
</instance>
<instance part="END32" gate="1" x="81.28" y="55.88" smashed="yes">
<attribute name="NAME" x="82.677" y="55.1942" size="1.778" layer="95"/>
<attribute name="VALUE" x="80.137" y="52.578" size="1.778" layer="96"/>
</instance>
<instance part="SDA" gate="1" x="66.04" y="33.02" smashed="yes" rot="R180">
<attribute name="NAME" x="62.103" y="33.7058" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="67.183" y="36.322" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="SCL" gate="1" x="66.04" y="35.56" smashed="yes" rot="R180">
<attribute name="NAME" x="62.103" y="36.2458" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="67.183" y="38.862" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="PCAMS_3.3V" gate="1" x="7.62" y="71.12"/>
<instance part="PCAMS_BMON" gate="1" x="127" y="50.8" rot="R180"/>
<instance part="PCAMS_X" gate="1" x="127" y="45.72" rot="R180"/>
<instance part="BNO_3.3V" gate="1" x="-43.18" y="60.96" smashed="yes">
<attribute name="NAME" x="-57.023" y="60.2742" size="1.778" layer="95"/>
<attribute name="VALUE" x="-44.323" y="57.658" size="1.778" layer="96"/>
</instance>
<instance part="BNO_GND" gate="1" x="-43.18" y="58.42" smashed="yes">
<attribute name="NAME" x="-57.023" y="57.7342" size="1.778" layer="95"/>
<attribute name="VALUE" x="-44.323" y="55.118" size="1.778" layer="96"/>
</instance>
<instance part="P_GND" gate="1" x="2.54" y="43.18" rot="R180"/>
<instance part="XSHUT_LEFT" gate="1" x="66.04" y="60.96" smashed="yes" rot="R180">
<attribute name="NAME" x="62.103" y="61.6458" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="67.183" y="64.262" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="XSHUT_RIGHT" gate="1" x="66.04" y="58.42" smashed="yes" rot="R180">
<attribute name="NAME" x="62.103" y="59.1058" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="67.183" y="61.722" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="RIGHT_TOF" gate="-1" x="25.4" y="45.72" rot="R180"/>
<instance part="RIGHT_TOF" gate="-2" x="25.4" y="48.26" rot="R180"/>
<instance part="RIGHT_TOF" gate="-3" x="25.4" y="50.8" rot="R180"/>
<instance part="MOC-SSIU" gate="-1" x="63.5" y="12.7" rot="R180"/>
<instance part="MOC-SSIU" gate="-2" x="63.5" y="10.16" rot="R180"/>
<instance part="MOC-SSIU" gate="-3" x="63.5" y="7.62" rot="R180"/>
<instance part="MOC-SSIU" gate="-4" x="63.5" y="5.08"/>
<instance part="MOC-SSIU" gate="-5" x="63.5" y="2.54"/>
<instance part="MOC-SSIU" gate="-6" x="63.5" y="0"/>
<instance part="MOC-SSIU" gate="-7" x="63.5" y="-2.54" rot="R180"/>
<instance part="MOC-SSIU" gate="-8" x="63.5" y="-5.08"/>
<instance part="FRONT_TOF" gate="-1" x="35.56" y="78.74" rot="R90"/>
<instance part="FRONT_TOF" gate="-2" x="33.02" y="78.74" rot="R90"/>
<instance part="FRONT_TOF" gate="-3" x="30.48" y="78.74" rot="R90"/>
<instance part="FRONT_TOF" gate="-4" x="27.94" y="78.74" rot="R90"/>
<instance part="FRONT_TOF" gate="-5" x="25.4" y="78.74" rot="R90"/>
<instance part="FRONT_TOF" gate="-6" x="22.86" y="78.74" rot="R90"/>
<instance part="FRONT_TOF" gate="-7" x="20.32" y="78.74" rot="R90"/>
<instance part="FRONT_TOF" gate="-8" x="17.78" y="78.74" rot="R90"/>
<instance part="P_RX" gate="1" x="48.26" y="48.26" smashed="yes" rot="R90">
<attribute name="NAME" x="54.0258" y="47.117" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="51.562" y="47.117" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P_TX" gate="1" x="43.18" y="48.26" smashed="yes" rot="R90">
<attribute name="NAME" x="38.7858" y="44.577" size="1.778" layer="95"/>
<attribute name="VALUE" x="46.482" y="47.117" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="BP_GND" gate="1" x="66.04" y="22.86" smashed="yes" rot="R180">
<attribute name="NAME" x="62.103" y="23.5458" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="67.183" y="26.162" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="BP_5V" gate="1" x="66.04" y="25.4" smashed="yes" rot="R180">
<attribute name="NAME" x="62.103" y="26.0858" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="67.183" y="28.702" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="BP_GND1" gate="1" x="81.28" y="66.04" smashed="yes">
<attribute name="NAME" x="85.217" y="65.3542" size="1.778" layer="95"/>
<attribute name="VALUE" x="80.137" y="62.738" size="1.778" layer="96"/>
</instance>
<instance part="BP_GND2" gate="1" x="81.28" y="68.58" smashed="yes">
<attribute name="NAME" x="85.217" y="67.8942" size="1.778" layer="95"/>
<attribute name="VALUE" x="80.137" y="65.278" size="1.778" layer="96"/>
</instance>
<instance part="BP_3.3V_" gate="1" x="81.28" y="63.5" smashed="yes">
<attribute name="NAME" x="85.217" y="62.8142" size="1.778" layer="95"/>
<attribute name="VALUE" x="80.137" y="60.198" size="1.778" layer="96"/>
</instance>
<instance part="BP_3.3V_1" gate="1" x="66.04" y="20.32" smashed="yes" rot="MR0">
<attribute name="NAME" x="62.103" y="19.6342" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="67.183" y="17.018" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="LEFT_TOF" gate="-1" x="25.4" y="35.56" rot="R180"/>
<instance part="LEFT_TOF" gate="-2" x="25.4" y="38.1" rot="R180"/>
<instance part="LEFT_TOF" gate="-3" x="25.4" y="40.64" rot="R180"/>
<instance part="JMP_TX" gate="1" x="17.78" y="55.88" smashed="yes">
<attribute name="NAME" x="29.337" y="57.7342" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="16.637" y="52.578" size="1.778" layer="96"/>
</instance>
<instance part="JMP_RX" gate="1" x="17.78" y="53.34" smashed="yes">
<attribute name="NAME" x="19.177" y="52.6542" size="1.778" layer="95"/>
<attribute name="VALUE" x="16.637" y="50.038" size="1.778" layer="96"/>
</instance>
<instance part="JMP_TX1" gate="1" x="12.7" y="55.88" smashed="yes" rot="R180">
<attribute name="NAME" x="1.143" y="56.5658" size="1.778" layer="95"/>
<attribute name="VALUE" x="13.843" y="59.182" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="JMP_RX1" gate="1" x="12.7" y="53.34" smashed="yes" rot="R180">
<attribute name="NAME" x="11.303" y="54.0258" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="13.843" y="56.642" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="VBAT" gate="1" x="81.28" y="20.32" smashed="yes">
<attribute name="NAME" x="82.677" y="19.6342" size="1.778" layer="95"/>
<attribute name="VALUE" x="80.137" y="17.018" size="1.778" layer="96"/>
</instance>
<instance part="LED" gate="1" x="81.28" y="22.86" smashed="yes">
<attribute name="NAME" x="82.677" y="22.1742" size="1.778" layer="95"/>
<attribute name="VALUE" x="80.137" y="19.558" size="1.778" layer="96"/>
</instance>
<instance part="OIN" gate="1" x="81.28" y="25.4" smashed="yes">
<attribute name="NAME" x="82.677" y="24.7142" size="1.778" layer="95"/>
<attribute name="VALUE" x="80.137" y="22.098" size="1.778" layer="96"/>
</instance>
<instance part="OOUT" gate="1" x="81.28" y="27.94" smashed="yes">
<attribute name="NAME" x="82.677" y="27.2542" size="1.778" layer="95"/>
<attribute name="VALUE" x="80.137" y="24.638" size="1.778" layer="96"/>
</instance>
<instance part="PWM1" gate="1" x="81.28" y="35.56" smashed="yes">
<attribute name="NAME" x="82.677" y="34.8742" size="1.778" layer="95"/>
<attribute name="VALUE" x="80.137" y="32.258" size="1.778" layer="96"/>
</instance>
<instance part="PWM2" gate="1" x="81.28" y="38.1" smashed="yes">
<attribute name="NAME" x="82.677" y="37.4142" size="1.778" layer="95"/>
<attribute name="VALUE" x="80.137" y="34.798" size="1.778" layer="96"/>
</instance>
<instance part="SS1" gate="1" x="81.28" y="40.64" smashed="yes">
<attribute name="NAME" x="82.677" y="39.9542" size="1.778" layer="95"/>
<attribute name="VALUE" x="80.137" y="37.338" size="1.778" layer="96"/>
</instance>
<instance part="SCK1" gate="1" x="81.28" y="43.18" smashed="yes">
<attribute name="NAME" x="82.677" y="42.4942" size="1.778" layer="95"/>
<attribute name="VALUE" x="80.137" y="39.878" size="1.778" layer="96"/>
</instance>
<instance part="MISO1" gate="1" x="81.28" y="45.72" smashed="yes">
<attribute name="NAME" x="82.677" y="45.0342" size="1.778" layer="95"/>
<attribute name="VALUE" x="80.137" y="42.418" size="1.778" layer="96"/>
</instance>
<instance part="MOSI1" gate="1" x="81.28" y="48.26" smashed="yes">
<attribute name="NAME" x="82.677" y="47.5742" size="1.778" layer="95"/>
<attribute name="VALUE" x="80.137" y="44.958" size="1.778" layer="96"/>
</instance>
<instance part="BMON" gate="1" x="81.28" y="50.8" smashed="yes">
<attribute name="NAME" x="82.677" y="50.1142" size="1.778" layer="95"/>
<attribute name="VALUE" x="80.137" y="47.498" size="1.778" layer="96"/>
</instance>
<instance part="RST" gate="1" x="101.6" y="60.96" smashed="yes">
<attribute name="NAME" x="102.997" y="60.2742" size="1.778" layer="95"/>
<attribute name="VALUE" x="100.457" y="57.658" size="1.778" layer="96"/>
</instance>
<instance part="PB1" gate="1" x="81.28" y="53.34" smashed="yes">
<attribute name="NAME" x="82.677" y="52.6542" size="1.778" layer="95"/>
<attribute name="VALUE" x="80.137" y="50.038" size="1.778" layer="96"/>
</instance>
<instance part="PB5" gate="1" x="66.04" y="38.1" smashed="yes" rot="R180">
<attribute name="NAME" x="62.103" y="38.7858" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="67.183" y="41.402" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="PB4" gate="1" x="66.04" y="40.64" smashed="yes" rot="R180">
<attribute name="NAME" x="62.103" y="41.3258" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="67.183" y="43.942" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="PB3" gate="1" x="66.04" y="43.18" smashed="yes" rot="R180">
<attribute name="NAME" x="62.103" y="43.8658" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="67.183" y="46.482" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="PA15" gate="1" x="66.04" y="45.72" smashed="yes" rot="R180">
<attribute name="NAME" x="62.103" y="46.4058" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="67.183" y="49.022" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="PA12" gate="1" x="66.04" y="48.26" smashed="yes" rot="R180">
<attribute name="NAME" x="62.103" y="48.9458" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="67.183" y="51.562" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="PA11" gate="1" x="66.04" y="50.8" smashed="yes" rot="R180">
<attribute name="NAME" x="62.103" y="51.4858" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="67.183" y="54.102" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="MOSI2" gate="1" x="101.6" y="48.26" smashed="yes">
<attribute name="NAME" x="102.997" y="47.5742" size="1.778" layer="95"/>
<attribute name="VALUE" x="100.457" y="44.958" size="1.778" layer="96"/>
</instance>
<instance part="MISO2" gate="1" x="101.6" y="45.72" smashed="yes">
<attribute name="NAME" x="102.997" y="45.0342" size="1.778" layer="95"/>
<attribute name="VALUE" x="100.457" y="42.418" size="1.778" layer="96"/>
</instance>
<instance part="SCK2" gate="1" x="101.6" y="43.18" smashed="yes">
<attribute name="NAME" x="102.997" y="42.4942" size="1.778" layer="95"/>
<attribute name="VALUE" x="100.457" y="39.878" size="1.778" layer="96"/>
</instance>
<instance part="SS2" gate="1" x="101.6" y="40.64" smashed="yes">
<attribute name="NAME" x="102.997" y="39.9542" size="1.778" layer="95"/>
<attribute name="VALUE" x="100.457" y="37.338" size="1.778" layer="96"/>
</instance>
<instance part="PWM2_" gate="1" x="101.6" y="38.1" smashed="yes">
<attribute name="NAME" x="102.997" y="37.4142" size="1.778" layer="95"/>
<attribute name="VALUE" x="100.457" y="34.798" size="1.778" layer="96"/>
</instance>
<instance part="PWM1_" gate="1" x="101.6" y="35.56" smashed="yes">
<attribute name="NAME" x="102.997" y="34.8742" size="1.778" layer="95"/>
<attribute name="VALUE" x="100.457" y="32.258" size="1.778" layer="96"/>
</instance>
<instance part="SCL1" gate="1" x="30.48" y="15.24" smashed="yes">
<attribute name="NAME" x="34.417" y="14.5542" size="1.778" layer="95"/>
<attribute name="VALUE" x="29.337" y="11.938" size="1.778" layer="96"/>
</instance>
<instance part="SDA1" gate="1" x="30.48" y="12.7" smashed="yes">
<attribute name="NAME" x="34.417" y="12.0142" size="1.778" layer="95"/>
<attribute name="VALUE" x="29.337" y="9.398" size="1.778" layer="96"/>
</instance>
<instance part="PI_SDA" gate="1" x="-45.72" y="76.2" smashed="yes">
<attribute name="NAME" x="-57.023" y="75.5142" size="1.778" layer="95"/>
<attribute name="VALUE" x="-46.863" y="72.898" size="1.778" layer="96"/>
</instance>
<instance part="PI_SCL" gate="1" x="-45.72" y="73.66" smashed="yes">
<attribute name="NAME" x="-57.023" y="72.9742" size="1.778" layer="95"/>
<attribute name="VALUE" x="-46.863" y="70.358" size="1.778" layer="96"/>
</instance>
<instance part="RST1" gate="1" x="81.28" y="60.96" smashed="yes">
<attribute name="NAME" x="82.677" y="60.2742" size="1.778" layer="95"/>
<attribute name="VALUE" x="80.137" y="57.658" size="1.778" layer="96"/>
</instance>
<instance part="5V_" gate="1" x="-2.54" y="25.4" smashed="yes">
<attribute name="NAME" x="-3.683" y="22.1742" size="1.778" layer="95"/>
<attribute name="VALUE" x="-3.683" y="22.098" size="1.778" layer="96"/>
</instance>
<instance part="GND_" gate="1" x="-2.54" y="20.32" smashed="yes">
<attribute name="NAME" x="1.397" y="19.6342" size="1.778" layer="95"/>
<attribute name="VALUE" x="-3.683" y="17.018" size="1.778" layer="96"/>
</instance>
<instance part="PA1" gate="1" x="71.12" y="45.72" smashed="yes" rot="R180">
<attribute name="NAME" x="67.183" y="46.4058" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="72.263" y="49.022" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="PB3_" gate="1" x="71.12" y="43.18" smashed="yes" rot="R180">
<attribute name="NAME" x="67.183" y="43.8658" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="72.263" y="46.482" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="PB4_" gate="1" x="71.12" y="40.64" smashed="yes" rot="R180">
<attribute name="NAME" x="67.183" y="41.3258" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="72.263" y="43.942" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND_1" gate="1" x="-2.54" y="15.24" smashed="yes">
<attribute name="NAME" x="1.397" y="14.5542" size="1.778" layer="95"/>
<attribute name="VALUE" x="-3.683" y="11.938" size="1.778" layer="96"/>
</instance>
<instance part="PCAMS_3.3V1" gate="1" x="7.62" y="66.04"/>
<instance part="PCAMS_5V1" gate="1" x="0" y="73.66" rot="R180"/>
<instance part="X" gate="1" x="-30.48" y="2.54" smashed="yes" rot="R90">
<attribute name="NAME" x="-29.7942" y="6.477" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-27.178" y="1.397" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="X1" gate="1" x="-27.94" y="2.54" smashed="yes" rot="R90">
<attribute name="NAME" x="-27.2542" y="6.477" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-24.638" y="1.397" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="X3" gate="1" x="-25.4" y="2.54" smashed="yes" rot="R90">
<attribute name="NAME" x="-24.7142" y="6.477" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-22.098" y="1.397" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="X4" gate="1" x="-22.86" y="2.54" smashed="yes" rot="R90">
<attribute name="NAME" x="-22.1742" y="6.477" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-19.558" y="1.397" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="X5" gate="1" x="-20.32" y="2.54" smashed="yes" rot="R90">
<attribute name="NAME" x="-19.6342" y="6.477" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-17.018" y="1.397" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="X6" gate="1" x="-17.78" y="2.54" smashed="yes" rot="R90">
<attribute name="NAME" x="-17.0942" y="6.477" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-14.478" y="1.397" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="X7" gate="1" x="-15.24" y="2.54" smashed="yes" rot="R90">
<attribute name="NAME" x="-14.5542" y="6.477" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-11.938" y="1.397" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="X8" gate="1" x="-12.7" y="2.54" smashed="yes" rot="R90">
<attribute name="NAME" x="-12.0142" y="6.477" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-9.398" y="1.397" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="X9" gate="1" x="-10.16" y="2.54" smashed="yes" rot="R90">
<attribute name="NAME" x="-9.4742" y="6.477" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-6.858" y="1.397" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND_2" gate="1" x="-2.54" y="17.78" smashed="yes">
<attribute name="NAME" x="1.397" y="17.0942" size="1.778" layer="95"/>
<attribute name="VALUE" x="-3.683" y="14.478" size="1.778" layer="96"/>
</instance>
<instance part="X2" gate="1" x="7.62" y="76.2" smashed="yes">
<attribute name="NAME" x="11.557" y="75.5142" size="1.778" layer="95"/>
<attribute name="VALUE" x="6.477" y="72.898" size="1.778" layer="96"/>
</instance>
<instance part="PB5_" gate="1" x="71.12" y="38.1" smashed="yes" rot="R180">
<attribute name="NAME" x="67.183" y="38.7858" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="72.263" y="41.402" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="PB1_" gate="1" x="101.6" y="53.34" smashed="yes">
<attribute name="NAME" x="102.997" y="52.6542" size="1.778" layer="95"/>
<attribute name="VALUE" x="100.457" y="50.038" size="1.778" layer="96"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="JP3" gate="A" pin="6"/>
<pinref part="PCAMS_GND" gate="1" pin="P"/>
<wire x1="-17.78" y1="63.5" x2="-10.16" y2="63.5" width="0.1524" layer="91"/>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="-10.16" y1="63.5" x2="-5.08" y2="63.5" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="63.5" x2="-2.54" y2="63.5" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="63.5" x2="-5.08" y2="53.34" width="0.1524" layer="91"/>
<junction x="-5.08" y="63.5"/>
<pinref part="BNO_GND" gate="1" pin="P"/>
<wire x1="-5.08" y1="53.34" x2="-5.08" y2="45.72" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="45.72" x2="-5.08" y2="43.18" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="43.18" x2="-5.08" y2="40.64" width="0.1524" layer="91"/>
<wire x1="-40.64" y1="58.42" x2="-33.02" y2="58.42" width="0.1524" layer="91"/>
<wire x1="-33.02" y1="58.42" x2="-33.02" y2="76.2" width="0.1524" layer="91"/>
<wire x1="-33.02" y1="76.2" x2="-10.16" y2="76.2" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="76.2" x2="-10.16" y2="63.5" width="0.1524" layer="91"/>
<junction x="-10.16" y="63.5"/>
<pinref part="P_GND" gate="1" pin="P"/>
<wire x1="0" y1="43.18" x2="-5.08" y2="43.18" width="0.1524" layer="91"/>
<junction x="-5.08" y="43.18"/>
<pinref part="JP3" gate="A" pin="9"/>
<wire x1="-25.4" y1="58.42" x2="-33.02" y2="58.42" width="0.1524" layer="91"/>
<junction x="-33.02" y="58.42"/>
<pinref part="JP3" gate="A" pin="14"/>
<wire x1="-17.78" y1="53.34" x2="-5.08" y2="53.34" width="0.1524" layer="91"/>
<junction x="-5.08" y="53.34"/>
<pinref part="JP3" gate="A" pin="20"/>
<wire x1="-17.78" y1="45.72" x2="-10.16" y2="45.72" width="0.1524" layer="91"/>
<junction x="-5.08" y="45.72"/>
<pinref part="JP3" gate="A" pin="25"/>
<wire x1="-10.16" y1="45.72" x2="-5.08" y2="45.72" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="38.1" x2="-33.02" y2="38.1" width="0.1524" layer="91"/>
<wire x1="-33.02" y1="38.1" x2="-33.02" y2="15.24" width="0.1524" layer="91"/>
<wire x1="-33.02" y1="15.24" x2="-25.4" y2="15.24" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="33.02" x2="-10.16" y2="45.72" width="0.1524" layer="91"/>
<junction x="-10.16" y="45.72"/>
<pinref part="JP3" gate="A" pin="30"/>
<wire x1="-17.78" y1="33.02" x2="-10.16" y2="33.02" width="0.1524" layer="91"/>
<pinref part="JP3" gate="A" pin="34"/>
<wire x1="-17.78" y1="27.94" x2="-10.16" y2="27.94" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="27.94" x2="-10.16" y2="33.02" width="0.1524" layer="91"/>
<junction x="-10.16" y="33.02"/>
<pinref part="JP3" gate="A" pin="39"/>
<wire x1="-25.4" y1="20.32" x2="-25.4" y2="15.24" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="15.24" x2="-10.16" y2="15.24" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="15.24" x2="-10.16" y2="27.94" width="0.1524" layer="91"/>
<junction x="-10.16" y="27.94"/>
<junction x="-25.4" y="15.24"/>
</segment>
<segment>
<pinref part="GND2" gate="1" pin="GND"/>
<wire x1="10.16" y1="22.86" x2="10.16" y2="20.32" width="0.1524" layer="91"/>
<pinref part="BP_GND" gate="1" pin="P"/>
<wire x1="10.16" y1="20.32" x2="10.16" y2="17.78" width="0.1524" layer="91"/>
<wire x1="10.16" y1="17.78" x2="10.16" y2="15.24" width="0.1524" layer="91"/>
<wire x1="10.16" y1="22.86" x2="63.5" y2="22.86" width="0.1524" layer="91"/>
<pinref part="GND_" gate="1" pin="P"/>
<wire x1="0" y1="20.32" x2="10.16" y2="20.32" width="0.1524" layer="91"/>
<junction x="10.16" y="20.32"/>
<pinref part="GND_1" gate="1" pin="P"/>
<wire x1="0" y1="15.24" x2="10.16" y2="15.24" width="0.1524" layer="91"/>
<junction x="10.16" y="15.24"/>
<pinref part="GND_2" gate="1" pin="P"/>
<wire x1="0" y1="17.78" x2="10.16" y2="17.78" width="0.1524" layer="91"/>
<junction x="10.16" y="17.78"/>
</segment>
<segment>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="111.76" y1="73.66" x2="111.76" y2="68.58" width="0.1524" layer="91"/>
<wire x1="111.76" y1="68.58" x2="111.76" y2="66.04" width="0.1524" layer="91"/>
<wire x1="111.76" y1="66.04" x2="111.76" y2="60.96" width="0.1524" layer="91"/>
<pinref part="BP_GND2" gate="1" pin="P"/>
<wire x1="83.82" y1="68.58" x2="111.76" y2="68.58" width="0.1524" layer="91"/>
<junction x="111.76" y="68.58"/>
<pinref part="BP_GND1" gate="1" pin="P"/>
<wire x1="83.82" y1="66.04" x2="111.76" y2="66.04" width="0.1524" layer="91"/>
<junction x="111.76" y="66.04"/>
<wire x1="22.86" y1="73.66" x2="111.76" y2="73.66" width="0.1524" layer="91"/>
<pinref part="FRONT_TOF" gate="-6" pin="S"/>
<wire x1="22.86" y1="76.2" x2="22.86" y2="73.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="TX" class="0">
<segment>
<pinref part="JP3" gate="A" pin="8"/>
<wire x1="-17.78" y1="60.96" x2="0" y2="60.96" width="0.1524" layer="91"/>
<wire x1="0" y1="60.96" x2="0" y2="53.34" width="0.1524" layer="91"/>
<pinref part="JMP_RX1" gate="1" pin="P"/>
<wire x1="0" y1="53.34" x2="10.16" y2="53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="RX" class="0">
<segment>
<pinref part="BP_TX" gate="1" pin="P"/>
<wire x1="20.32" y1="55.88" x2="43.18" y2="55.88" width="0.1524" layer="91"/>
<pinref part="JMP_TX" gate="1" pin="P"/>
<pinref part="P_TX" gate="1" pin="P"/>
<wire x1="43.18" y1="55.88" x2="63.5" y2="55.88" width="0.1524" layer="91"/>
<wire x1="43.18" y1="50.8" x2="43.18" y2="55.88" width="0.1524" layer="91"/>
<junction x="43.18" y="55.88"/>
</segment>
</net>
<net name="SDA" class="0">
<segment>
<pinref part="SDA" gate="1" pin="P"/>
<wire x1="63.5" y1="33.02" x2="40.64" y2="33.02" width="0.1524" layer="91"/>
<wire x1="40.64" y1="33.02" x2="40.64" y2="12.7" width="0.1524" layer="91"/>
<pinref part="MOC-SSIU" gate="-6" pin="S"/>
<wire x1="40.64" y1="12.7" x2="40.64" y2="0" width="0.1524" layer="91"/>
<wire x1="40.64" y1="0" x2="60.96" y2="0" width="0.1524" layer="91"/>
<wire x1="35.56" y1="33.02" x2="40.64" y2="33.02" width="0.1524" layer="91"/>
<junction x="40.64" y="33.02"/>
<wire x1="35.56" y1="33.02" x2="35.56" y2="40.64" width="0.1524" layer="91"/>
<pinref part="RIGHT_TOF" gate="-3" pin="S"/>
<wire x1="35.56" y1="40.64" x2="35.56" y2="50.8" width="0.1524" layer="91"/>
<wire x1="27.94" y1="50.8" x2="35.56" y2="50.8" width="0.1524" layer="91"/>
<pinref part="LEFT_TOF" gate="-3" pin="S"/>
<wire x1="27.94" y1="40.64" x2="35.56" y2="40.64" width="0.1524" layer="91"/>
<junction x="35.56" y="40.64"/>
<pinref part="FRONT_TOF" gate="-1" pin="S"/>
<wire x1="35.56" y1="50.8" x2="35.56" y2="76.2" width="0.1524" layer="91"/>
<junction x="35.56" y="50.8"/>
<pinref part="SDA1" gate="1" pin="P"/>
<wire x1="33.02" y1="12.7" x2="40.64" y2="12.7" width="0.1524" layer="91"/>
<junction x="40.64" y="12.7"/>
</segment>
</net>
<net name="SCL" class="0">
<segment>
<pinref part="SCL" gate="1" pin="P"/>
<wire x1="63.5" y1="35.56" x2="45.72" y2="35.56" width="0.1524" layer="91"/>
<wire x1="45.72" y1="35.56" x2="45.72" y2="15.24" width="0.1524" layer="91"/>
<pinref part="MOC-SSIU" gate="-5" pin="S"/>
<wire x1="45.72" y1="15.24" x2="45.72" y2="2.54" width="0.1524" layer="91"/>
<wire x1="45.72" y1="2.54" x2="60.96" y2="2.54" width="0.1524" layer="91"/>
<wire x1="33.02" y1="35.56" x2="45.72" y2="35.56" width="0.1524" layer="91"/>
<junction x="45.72" y="35.56"/>
<wire x1="33.02" y1="35.56" x2="33.02" y2="38.1" width="0.1524" layer="91"/>
<pinref part="RIGHT_TOF" gate="-2" pin="S"/>
<wire x1="33.02" y1="38.1" x2="33.02" y2="48.26" width="0.1524" layer="91"/>
<wire x1="27.94" y1="48.26" x2="33.02" y2="48.26" width="0.1524" layer="91"/>
<pinref part="LEFT_TOF" gate="-2" pin="S"/>
<wire x1="27.94" y1="38.1" x2="33.02" y2="38.1" width="0.1524" layer="91"/>
<junction x="33.02" y="38.1"/>
<pinref part="FRONT_TOF" gate="-2" pin="S"/>
<wire x1="33.02" y1="76.2" x2="33.02" y2="48.26" width="0.1524" layer="91"/>
<junction x="33.02" y="48.26"/>
<pinref part="SCL1" gate="1" pin="P"/>
<wire x1="33.02" y1="15.24" x2="45.72" y2="15.24" width="0.1524" layer="91"/>
<junction x="45.72" y="15.24"/>
</segment>
</net>
<net name="5V" class="0">
<segment>
<pinref part="JP3" gate="A" pin="2"/>
<pinref part="PCAMS_5V" gate="1" pin="P"/>
<wire x1="-17.78" y1="68.58" x2="-5.08" y2="68.58" width="0.1524" layer="91"/>
<pinref part="P+1" gate="VCC" pin="VCC"/>
<wire x1="-5.08" y1="68.58" x2="-2.54" y2="68.58" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="81.28" x2="-5.08" y2="73.66" width="0.1524" layer="91"/>
<junction x="-5.08" y="68.58"/>
<pinref part="PCAMS_5V1" gate="1" pin="P"/>
<wire x1="-5.08" y1="73.66" x2="-5.08" y2="68.58" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="73.66" x2="-5.08" y2="73.66" width="0.1524" layer="91"/>
<junction x="-5.08" y="73.66"/>
</segment>
<segment>
<pinref part="P+2" gate="VCC" pin="VCC"/>
<wire x1="10.16" y1="27.94" x2="10.16" y2="25.4" width="0.1524" layer="91"/>
<pinref part="BP_5V" gate="1" pin="P"/>
<wire x1="63.5" y1="25.4" x2="10.16" y2="25.4" width="0.1524" layer="91"/>
<pinref part="5V_" gate="1" pin="P"/>
<wire x1="0" y1="25.4" x2="10.16" y2="25.4" width="0.1524" layer="91"/>
<junction x="10.16" y="25.4"/>
</segment>
</net>
<net name="PI_SCL" class="0">
<segment>
<pinref part="BNO_SCL" gate="1" pin="P"/>
<pinref part="JP3" gate="A" pin="5"/>
<wire x1="-25.4" y1="63.5" x2="-40.64" y2="63.5" width="0.1524" layer="91"/>
<pinref part="PI_SCL" gate="1" pin="P"/>
<wire x1="-43.18" y1="73.66" x2="-40.64" y2="73.66" width="0.1524" layer="91"/>
<wire x1="-40.64" y1="73.66" x2="-40.64" y2="63.5" width="0.1524" layer="91"/>
<junction x="-40.64" y="63.5"/>
</segment>
</net>
<net name="RPI_3.3V" class="0">
<segment>
<pinref part="BNO_3.3V" gate="1" pin="P"/>
<wire x1="-40.64" y1="60.96" x2="-35.56" y2="60.96" width="0.1524" layer="91"/>
<wire x1="-35.56" y1="60.96" x2="-35.56" y2="68.58" width="0.1524" layer="91"/>
<pinref part="JP3" gate="A" pin="1"/>
<wire x1="-35.56" y1="68.58" x2="-25.4" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="BP_3.3V" class="0">
<segment>
<pinref part="BP_3.3V_" gate="1" pin="P"/>
<wire x1="83.82" y1="63.5" x2="86.36" y2="63.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PCAMS_3.3V" class="0">
<segment>
<pinref part="PCAMS_3.3V" gate="1" pin="P"/>
<wire x1="20.32" y1="71.12" x2="12.7" y2="71.12" width="0.1524" layer="91"/>
<pinref part="FRONT_TOF" gate="-7" pin="S"/>
<wire x1="12.7" y1="71.12" x2="10.16" y2="71.12" width="0.1524" layer="91"/>
<wire x1="20.32" y1="71.12" x2="20.32" y2="76.2" width="0.1524" layer="91"/>
<pinref part="PCAMS_3.3V1" gate="1" pin="P"/>
<wire x1="10.16" y1="66.04" x2="12.7" y2="66.04" width="0.1524" layer="91"/>
<wire x1="12.7" y1="66.04" x2="12.7" y2="71.12" width="0.1524" layer="91"/>
<junction x="12.7" y="71.12"/>
</segment>
</net>
<net name="END31" class="0">
<segment>
<pinref part="END31" gate="1" pin="P"/>
<wire x1="83.82" y1="58.42" x2="99.06" y2="58.42" width="0.1524" layer="91"/>
<wire x1="99.06" y1="58.42" x2="99.06" y2="12.7" width="0.1524" layer="91"/>
<wire x1="99.06" y1="12.7" x2="66.04" y2="12.7" width="0.1524" layer="91"/>
<pinref part="MOC-SSIU" gate="-1" pin="S"/>
</segment>
</net>
<net name="END32" class="0">
<segment>
<pinref part="END32" gate="1" pin="P"/>
<wire x1="83.82" y1="55.88" x2="96.52" y2="55.88" width="0.1524" layer="91"/>
<wire x1="96.52" y1="55.88" x2="96.52" y2="10.16" width="0.1524" layer="91"/>
<wire x1="96.52" y1="10.16" x2="66.04" y2="10.16" width="0.1524" layer="91"/>
<pinref part="MOC-SSIU" gate="-2" pin="S"/>
</segment>
</net>
<net name="ENC11" class="0">
<segment>
<pinref part="ENC11" gate="1" pin="P"/>
<wire x1="83.82" y1="30.48" x2="91.44" y2="30.48" width="0.1524" layer="91"/>
<wire x1="91.44" y1="30.48" x2="91.44" y2="7.62" width="0.1524" layer="91"/>
<pinref part="MOC-SSIU" gate="-3" pin="S"/>
<wire x1="91.44" y1="7.62" x2="66.04" y2="7.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ENC12" class="0">
<segment>
<pinref part="ENC12" gate="1" pin="P"/>
<wire x1="63.5" y1="27.94" x2="50.8" y2="27.94" width="0.1524" layer="91"/>
<wire x1="50.8" y1="27.94" x2="50.8" y2="5.08" width="0.1524" layer="91"/>
<pinref part="MOC-SSIU" gate="-4" pin="S"/>
<wire x1="50.8" y1="5.08" x2="60.96" y2="5.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ENC22" class="0">
<segment>
<pinref part="ENC22" gate="1" pin="P"/>
<wire x1="83.82" y1="33.02" x2="93.98" y2="33.02" width="0.1524" layer="91"/>
<wire x1="93.98" y1="33.02" x2="93.98" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="MOC-SSIU" gate="-7" pin="S"/>
<wire x1="93.98" y1="-2.54" x2="66.04" y2="-2.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ENC21" class="0">
<segment>
<pinref part="ENC21" gate="1" pin="P"/>
<wire x1="63.5" y1="30.48" x2="55.88" y2="30.48" width="0.1524" layer="91"/>
<wire x1="55.88" y1="30.48" x2="55.88" y2="-5.08" width="0.1524" layer="91"/>
<pinref part="MOC-SSIU" gate="-8" pin="S"/>
<wire x1="55.88" y1="-5.08" x2="60.96" y2="-5.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<wire x1="63.5" y1="68.58" x2="30.48" y2="68.58" width="0.1524" layer="91"/>
<pinref part="XSHUT_R" gate="1" pin="P"/>
<pinref part="FRONT_TOF" gate="-3" pin="S"/>
<wire x1="30.48" y1="76.2" x2="30.48" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<wire x1="63.5" y1="66.04" x2="27.94" y2="66.04" width="0.1524" layer="91"/>
<pinref part="XSHUT_M" gate="1" pin="P"/>
<pinref part="FRONT_TOF" gate="-4" pin="S"/>
<wire x1="27.94" y1="66.04" x2="27.94" y2="76.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="XSHUT_L" gate="1" pin="P"/>
<wire x1="63.5" y1="63.5" x2="25.4" y2="63.5" width="0.1524" layer="91"/>
<pinref part="FRONT_TOF" gate="-5" pin="S"/>
<wire x1="25.4" y1="63.5" x2="25.4" y2="76.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="XSHUT_RIGHT" class="0">
<segment>
<pinref part="XSHUT_RIGHT" gate="1" pin="P"/>
<wire x1="63.5" y1="58.42" x2="38.1" y2="58.42" width="0.1524" layer="91"/>
<wire x1="38.1" y1="58.42" x2="38.1" y2="45.72" width="0.1524" layer="91"/>
<pinref part="RIGHT_TOF" gate="-1" pin="S"/>
<wire x1="38.1" y1="45.72" x2="27.94" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="XSHUT_LEFT" class="0">
<segment>
<pinref part="XSHUT_LEFT" gate="1" pin="P"/>
<wire x1="63.5" y1="60.96" x2="30.48" y2="60.96" width="0.1524" layer="91"/>
<wire x1="30.48" y1="60.96" x2="30.48" y2="35.56" width="0.1524" layer="91"/>
<pinref part="LEFT_TOF" gate="-1" pin="S"/>
<wire x1="30.48" y1="35.56" x2="27.94" y2="35.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="JP3" gate="A" pin="10"/>
<wire x1="-17.78" y1="58.42" x2="-7.62" y2="58.42" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="58.42" x2="-7.62" y2="55.88" width="0.1524" layer="91"/>
<pinref part="JMP_TX1" gate="1" pin="P"/>
<wire x1="-7.62" y1="55.88" x2="10.16" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="BP_RX" gate="1" pin="P"/>
<pinref part="JMP_RX" gate="1" pin="P"/>
<wire x1="63.5" y1="53.34" x2="48.26" y2="53.34" width="0.1524" layer="91"/>
<pinref part="P_RX" gate="1" pin="P"/>
<wire x1="48.26" y1="53.34" x2="20.32" y2="53.34" width="0.1524" layer="91"/>
<wire x1="48.26" y1="50.8" x2="48.26" y2="53.34" width="0.1524" layer="91"/>
<junction x="48.26" y="53.34"/>
</segment>
</net>
<net name="PCAMS_BMON" class="0">
<segment>
<pinref part="BMON" gate="1" pin="P"/>
<wire x1="83.82" y1="50.8" x2="124.46" y2="50.8" width="0.1524" layer="91"/>
<pinref part="PCAMS_BMON" gate="1" pin="P"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="MOSI1" gate="1" pin="P"/>
<pinref part="MOSI2" gate="1" pin="P"/>
<wire x1="83.82" y1="48.26" x2="104.14" y2="48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="MISO1" gate="1" pin="P"/>
<pinref part="MISO2" gate="1" pin="P"/>
<wire x1="83.82" y1="45.72" x2="104.14" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="SCK1" gate="1" pin="P"/>
<pinref part="SCK2" gate="1" pin="P"/>
<wire x1="83.82" y1="43.18" x2="104.14" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="SS1" gate="1" pin="P"/>
<pinref part="SS2" gate="1" pin="P"/>
<wire x1="83.82" y1="40.64" x2="104.14" y2="40.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="PWM2" gate="1" pin="P"/>
<pinref part="PWM2_" gate="1" pin="P"/>
<wire x1="83.82" y1="38.1" x2="104.14" y2="38.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="PWM1" gate="1" pin="P"/>
<pinref part="PWM1_" gate="1" pin="P"/>
<wire x1="83.82" y1="35.56" x2="104.14" y2="35.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PI_SDA" class="0">
<segment>
<pinref part="PI_SDA" gate="1" pin="P"/>
<wire x1="-43.18" y1="76.2" x2="-38.1" y2="76.2" width="0.1524" layer="91"/>
<pinref part="BNO_SDA" gate="1" pin="P"/>
<pinref part="JP3" gate="A" pin="3"/>
<wire x1="-25.4" y1="66.04" x2="-38.1" y2="66.04" width="0.1524" layer="91"/>
<wire x1="-38.1" y1="66.04" x2="-40.64" y2="66.04" width="0.1524" layer="91"/>
<wire x1="-38.1" y1="76.2" x2="-38.1" y2="66.04" width="0.1524" layer="91"/>
<junction x="-38.1" y="66.04"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="RST" gate="1" pin="P"/>
<pinref part="RST1" gate="1" pin="P"/>
<wire x1="104.14" y1="60.96" x2="83.82" y2="60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="PA1" gate="1" pin="P"/>
<pinref part="PA15" gate="1" pin="P"/>
<wire x1="68.58" y1="45.72" x2="63.5" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="PB3_" gate="1" pin="P"/>
<pinref part="PB3" gate="1" pin="P"/>
<wire x1="68.58" y1="43.18" x2="63.5" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="PB4_" gate="1" pin="P"/>
<pinref part="PB4" gate="1" pin="P"/>
<wire x1="68.58" y1="40.64" x2="63.5" y2="40.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="X2" gate="1" pin="P"/>
<pinref part="FRONT_TOF" gate="-8" pin="S"/>
<wire x1="10.16" y1="76.2" x2="17.78" y2="76.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="PB5" gate="1" pin="P"/>
<pinref part="PB5_" gate="1" pin="P"/>
<wire x1="63.5" y1="38.1" x2="68.58" y2="38.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="PB1_" gate="1" pin="P"/>
<pinref part="PB1" gate="1" pin="P"/>
<wire x1="104.14" y1="53.34" x2="83.82" y2="53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<wire x1="-10.16" y1="20.32" x2="-10.16" y2="38.1" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
