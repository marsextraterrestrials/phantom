
- `ir_sensor`: PCB for ITR8307 infrared sensor

- `moc-ssiu`: **MO**tor **C**ontrol and **S**peed **S**ignal **I**ntegrator **U**nit

    - a PCA9685 I<sup>2</sup>C 16-channel PWM controller chip
    - three 2-channel DRV8833 motor controller boards
    - an LM339 comparator to handle speed sensor signals
    - connectors for up to six DC motors (and their corresponding sensors)
    - a connector to CREMCOAM-2 for I<sup>2</sup>C and sensor output
    - power connector pins at the bottom (this will sit on top of P-CAMS-2
    - some “spare” connectors and extra pins for any future ad-hoc extensions, just in case

- `cremcoam-2`: **C**entral **RE**lay **M**odule for **C**onnection **O**f **A**ll **M**odules **2**

    - a “hat” to sit on the Raspberry Pi, with the STM32 “blue pill” on top
    - connectors for ToF sensors
    - connector for P-CAMS
    - connector for MOC-SSIU
    - connector for thge BNO055
    - connector (and jumpers) for serial to program the STM32
    - optional extra pins to connect the remaining free STM32 pins (e.g. SPI, PWM)

- `p-cams-2`: **P**ower **C**onversion **A**nd **M**anagement **S**ystem **2**

    - connector and main power switch for 2S 16850 batteries
    - one adjustable DC-DC converter (set to 6V output)
    - one 5V DC-DC converter
    - one AMS1117 3.3V regulator
    - a voltage divider to be able to monitor battery voltage from a microcontroller

- `pdenes.lbr`: Eagle library with ITR8307 component
