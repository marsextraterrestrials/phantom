# P-CAMS-2: Power Conversion And Management System 2

This is the “power module” to provide the various voltages for the whole system: 5V for the Raspberry Pi and the STM32 microcontroller (“bluepill”), 6V for DC motors (and optionally servos), and 3.3V for additional electronics (sensors, etc.). This version was built on a perfboard (hence the “messy” board layout with lots of wires).

![](p-cams-2-brd.png)

![](p-cams-2-sch.png)

## 5V + 6V DC-DC converters

The circuit is mainly based on answers to these questions on electronics.stackexchange.com:

- [2 DC Motors and 18 Micro Servo motors from shared battery (6 AA batteries?), active monitoring?](https://electronics.stackexchange.com/questions/256093/2-dc-motors-and-18-micro-servo-motors-from-shared-battery-6-aa-batteries-act)
- [Can I power two linear DC regulators, each running a motor, from a single 12v source?](https://electronics.stackexchange.com/questions/245670/can-i-power-two-linear-dc-regulators-each-running-a-motor-from-a-single-12v-so?rq=1)

These talk about the need of an *isolating diode* (1N4001) before the DC converter (in the second case the explanation is *“to prevent voltage sag caused by one motor from discharging the capacitor on the other motor”*) for the MCU and a *large reservoir capacitor on the MCU side* (1000uF).

## AMS1117 3.3V circuit

       |
    +-----+
    |     |
    +-----+
    |  |  |
    G Out In
    |  |
    |  +---+
    |      |
    +-[ |]-+  22uF SMT tantalum capacitor (the + end is marked)

The capacitor choice is based on the answer to this question: [Capacitors for AMS1117 for 5V to 3.3V regulation](https://electronics.stackexchange.com/questions/200565/capacitors-for-ams1117-for-5v-to-3-3v-regulation).

## Connectors

* Input (`BAT-7.4V`): 2S 16850 batteries
* `MOT_6V` and `MOT_GND` 6V to motors (unused)
* `MOC-SSIU_CONN`: double pin header connector that provides 5V + 3.3V + GND to `MOC-SSIU` (that sits on top of P-CMAS-2)
* `CREMCOAM-2_CONN`: the “power” connector (5-pin header) to CREMCOAM: supplies 5V and 3.3V and the voltage from the voltage divider (roughly 1/3 of the battery voltage, so < 3V) for battery voltage monitoring. (The “X” pin is unused.)

