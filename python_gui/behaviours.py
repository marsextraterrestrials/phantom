from collections import Iterable
from math import floor
import numpy as np
from control_logic.utils import square2circle, speed_dir_from_joystick, pol2cart, cart2pol, sum_vectors

DIST_SENSOR_ANGLES = [-np.pi/2, -np.pi/4, 0, np.pi/4, np.pi/2]

"""
A collection of "behaviours".
These are functions that control the robot on a high level.
The current implementation is a set of very basic examples for the Pi Wars 2019 challenges.

A behaviour is a very simple concept. It's a single generator function that takes a single "state" parameter. First, it initialises whatever
it needs in the state, then runs either in an infinite loop or until some end condition, yield'ing in each step: so the behaviour function
"generates" the steps of the behaviour. Between steps, the contents of "state" is expected to be updated from the outside.

Behaviours can also be trivially composed of other, maybe lower level or reusable, behaviours by simply calling other functions.

Behaviours interact with the robot - or the rest of the system and potentially with each other, if using composition - by looking up 
and changing values in the "state" dict they are given.

The "state" given to the behaviour functions holds everything about the current known state of the robot. This is a simple
dictionary with values like "joy_x" (x axis position of the joystick), "speed" (the current desired speed set by a behaviour), 
"dir" (desired turn angle), "rpm" (the actual measured RPM of each motor), "heading" (actual measured heading based on the compass), etc.
It's a mixture of "inputs" and "outputs" - there is no distinction between the two: some behaviours will only read some values, others might
set or modify some of them. This might sound too simplistic or chaotic, but it makes it really easy to connect various parts of the system together,
and to implement high-level logic as a set of simple functions. Having the whole state of the system in one place
also makes things like logging / replaying / debugging / visualising very easy. All this is good enough for now for simple functionality,
but can be refined and developed in the future to maybe separate "reads" and "writes", express changes to the state and interactions between
multiple behaviours in a better way, maybe something similar to Subsumption Architecture.

"""

MIN_REMOTE_SPEED = 4
REMOTE_SPEED_MUL = (10 - MIN_REMOTE_SPEED)/10.0

def run_remote_ctrl():
    """
    Run the "remote control" behaviour.
    Drives (by setting 'speed' and 'dir') from the joystick input ('orig_joy_x' and 'orig_joy_y').
    """
    state = yield
    state['dist_measurements_on'] = False
    state['camera_on'] = False
    state['msgs'].append('※※※ remote_ctrl ※※※')
    state['start_heading'] = None
    state['set_heading'] = None
    state['dist_sum'] = (0,0)
    while True:
        (state['joy_x'], state['joy_y']) = square2circle(state['orig_joy_x'], state['orig_joy_y'])
        speed, dir = speed_dir_from_joystick(state['joy_x'], state['joy_y'])
        # apply min. speed:
        if speed > 0:
            speed = floor(speed * REMOTE_SPEED_MUL + MIN_REMOTE_SPEED)
        elif speed < 0:
            speed = floor(speed * REMOTE_SPEED_MUL - MIN_REMOTE_SPEED)
        state['speed'] = speed
        state['dir'] = dir
        state = yield
    
def run_canyons():
    """
    Run "Canyons of Mars".
    """
    state = yield
    state['msgs'].append('□□□ canyons □□□')
    state['set_heading'] = None
    state['dist_measurements_on'] = True
    state['camera_on'] = False
    while not state.get('dist', None):
        # distance measurements not ready yet, do nothing
        state = yield
    while True:
        state['speed'] = 7
        # add up the distance vectors (ignoring the front sensor)
        dists = state.get('dist', None)
        dist_vectors = [(dists[0], DIST_SENSOR_ANGLES[0]),
                        (dists[1], DIST_SENSOR_ANGLES[1]),
                        (dists[3], DIST_SENSOR_ANGLES[3]),
                        (dists[4], DIST_SENSOR_ANGLES[4])]
        dist_sum = sum_vectors(dist_vectors)
        state['dist_sum'] = dist_sum
        # turn towards this sum (the longest opening)
        state['dir'] = np.rad2deg(dist_sum[1]) * 1.8
        state = yield
    
def run_dist_vector_sum(state):
    """
    Turn towards the sum of the distance vectors.
    """
    dists = state['dist']
    dist_vectors = [(d, DIST_SENSOR_ANGLES[i]) for i, d in enumerate(dists)]
    dist_sum = sum_vectors(dist_vectors)
    state['dist_sum'] = dist_sum
    state['dir'] = np.rad2deg(dist_sum[1]) * 1.65

def run_dist_vector_45_sum(state):
    """
    Same as run_dist_vector_sum above, but only uses the 2 front 45 degree vectors, and limits the turn arc to be "gentle"
    """
    dists = state['dist']
    dist_vectors = [(dists[1], DIST_SENSOR_ANGLES[1]), (dists[3], DIST_SENSOR_ANGLES[3])]
    dist_sum = sum_vectors(dist_vectors)
    state['dist_sum'] = dist_sum
    state['dir'] = max(-45, min(45, np.rad2deg(dist_sum[1]) * 2))

def run_blast_off():
    """
    Run "Blast Off".
    """
    state = yield
    state['msgs'].append('△△△ blast_off △△△')
    state['set_heading'] = None
    state['dist_measurements_on'] = True
    state['camera_on'] = False
    while not state.get('dist', None):
        state = yield
    state['speed'] = 10
    while True:
        state = yield
        run_dist_vector_45_sum(state)
    
def add_angles(a, b):
    """
    Add two angles (positive or negative), and return the sum as an angle between 0 and 360.
    """
    return (a + b + 360) % 360

def get_rel_set_heading(state):
    """
    Return the relative set heading (relative to our current heading): -180..180
    """
    return (state['heading'] - state['set_heading'] + 360) % 360

HUBBLE_SPEED = 3
CORNER_MIN_DIST = 15
CORNER_MAX_DIST = 62

def run_turn():
    """
    Turn in place towards "set_heading".
    """
    state = yield
    state['msgs'].append('• start turn to {}° •'.format(state['set_heading']))
    # are we close enough?
    while abs(state['heading'] - state['set_heading']) > 3:
        rel_set_heading = get_rel_set_heading(state)
        if rel_set_heading < 180:
            state['dir'] = -90
        else:
            state['dir'] = 90
        state = yield
    state['msgs'].append('• end turn to {}° •'.format(state['set_heading']))

def run_hubble():
    """
    Run "The Hubble Telescope Nebula Challenge".
    """
    state = yield
    state['msgs'].append('○○○ hubble ○○○')
    state['dist_measurements_on'] = True
    heading = state['heading']
    state['start_heading'] = heading
    for angle in range(45, 360, 90):

        # turn to next corner
        state['set_heading'] = add_angles(state['start_heading'], angle)
        state['speed'] = HUBBLE_SPEED
        yield from run_turn()

        # go to corner
        state['msgs'].append('• go to corner at {}° •'.format(state['set_heading']))
        state['speed'] = HUBBLE_SPEED
        while state['dist'][2] >= CORNER_MIN_DIST:
            run_dist_vector_45_sum(state)
            state = yield

        # back to center
        state['msgs'].append('• back to center •')
        state['speed'] = -HUBBLE_SPEED
        while state['dist'][2] <= CORNER_MAX_DIST:
            run_dist_vector_45_sum(state)
            state = yield

    state['speed'] = 0
    state['msgs'].append('• end •')
