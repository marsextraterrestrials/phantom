#
# Simulated version of the "bluepill" module (interface to the onboard STM32 microcontroller).
# Instead of talking to the microcontroller, this version "drives" a Robot instance and gets distance measurements from it.
#

from threading import Thread, Timer
from queue import Queue
import time, random

queue = Queue()
drive_queue = Queue()
robot = None
# delay between measurements and reporting
dist_measurement_delay = 0
# frequency of reporting (must be >= dist_measurement_delay)
dist_measurement_freq = 0.1
# delay for the motor drive command
motor_drive_delay = 0.1

MAX_RPM = 200

def start(_robot):
    global robot
    robot = _robot

    for thread in [Thread(target = dist_loop), Thread(target = rpm_loop), Thread(target = drive_loop)]:
        thread.daemon = True
        thread.start()
    
def get_last_update():
    if not queue.empty():
        return queue.get_nowait()
    else:
        return None

def dist_loop():
    while True:
        dist = [min(99, int(d*100)) for d in robot.distances]
        time.sleep(dist_measurement_delay)
        queue.put({
            'dist': dist
        })
        time.sleep(dist_measurement_freq - dist_measurement_delay)

def rpm_loop():
    while True:
        queue.put({
            'rpm': (robot.left_rpm, robot.right_rpm, 0, 0)
        })
        time.sleep(0.1)

def drive_loop():
    while True:
        left, right = drive_queue.get()
        while not drive_queue.empty():
            left, right = drive_queue.get_nowait()
        robot.left_motor_speed = left
        robot.right_motor_speed = right
        time.sleep(motor_drive_delay)
        
def drive_motors(left, right):
    if motor_drive_delay > 0:
        drive_queue.put((left, right))
    else:
        robot.left_motor_speed = left
        robot.right_motor_speed = right
    
    
def distance_measurement(dist_on):
    robot.distance_measurement(dist_on)
