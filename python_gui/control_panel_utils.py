import pygame
import numpy as np
import pygame_utils as pgu

def draw_motor_graph(screen, pos, box_size, state, left_rpm_hist, right_rpm_hist, left_set_rpm_hist, right_set_rpm_hist, left_power_hist, right_power_hist, max_rpm, max_power, append=True):
    if append:
        left = state.get('left', 0)
        right = state.get('right', 0)
        rpm = state.get('rpm', (0,0,0,0))
        left_set_rpm = round(max_rpm * left / 10)
        right_set_rpm = round(max_rpm * right / 10)
        left_rpm_hist.append(rpm[0])
        right_rpm_hist.append(rpm[1])
        left_set_rpm_hist.append(left_set_rpm)
        right_set_rpm_hist.append(right_set_rpm)
        left_power_hist.append(state.get('m_left_power', 0))
        right_power_hist.append(state.get('m_right_power', 0))

    rect1 = pygame.Rect(pos, box_size)
    pgu.draw.plot(screen, rect1, (-max_rpm, max_rpm), left_set_rpm_hist, color=pgu.GREEN, border_color=pgu.GRAY, show_current_value=True, title='Left Motor Set RPM / RPM / Power')
    pgu.draw.plot(screen, rect1, (-max_rpm, max_rpm), left_rpm_hist, color=pgu.YELLOW, border_color=pgu.GRAY, show_current_value=True, current_value_offset = (0,20))
    pgu.draw.plot(screen, rect1, (-max_power, max_power), left_power_hist, color=pgu.RED, border_color=pgu.GRAY, show_current_value=True, current_value_offset = (0,40))

    pos2 = (pos[0] + box_size[0] + 50, pos[1])
    rect2 = pygame.Rect(pos2, box_size)
    pgu.draw.plot(screen, rect2, (-max_rpm, max_rpm), right_set_rpm_hist, color=pgu.GREEN, border_color=pgu.GRAY, show_current_value=True, title='Right Motor Set RPM / RPM / Power')
    pgu.draw.plot(screen, rect2, (-max_rpm, max_rpm), right_rpm_hist, color=pgu.YELLOW, border_color=pgu.GRAY, show_current_value=True, current_value_offset = (0,20))
    pgu.draw.plot(screen, rect2, (-max_power, max_power), right_power_hist, color=pgu.RED, border_color=pgu.GRAY, show_current_value=True, current_value_offset = (0,40))

def draw_dists(screen, dists_orig, r, state):
    dist = state.get('dist', (99,99,99,99,99))
    pygame.draw.arc(screen, pgu.GRAY, (dists_orig[0]-r-1, dists_orig[1]-r, r*2, r*2), 0, np.deg2rad(180.5)) # odd: 180 won't produce a full half-circle...
    m = r/99
    pgu.draw.vect(screen, dists_orig, (min(r, dist[0] * m), np.deg2rad(-90)), color=pgu.BLUE, vertical=True, font=pgu.mono, text_offset=(-80,0), text=str(dist[0]))
    pgu.draw.vect(screen, dists_orig, (min(r, dist[1] * m), np.deg2rad(-45)), color=pgu.BLUE, vertical=True, font=pgu.mono, text_offset=(-40,0), text=str(dist[1]))
    pgu.draw.vect(screen, dists_orig, (min(r, dist[2] * m), np.deg2rad(0)), color=pgu.BLUE, vertical=True, font=pgu.mono, text=str(dist[2]))
    pgu.draw.vect(screen, dists_orig, (min(r, dist[3] * m), np.deg2rad(45)), color=pgu.BLUE, vertical=True, font=pgu.mono, text_offset=(40,0), text=str(dist[3]))
    pgu.draw.vect(screen, dists_orig, (min(r, dist[4] * m), np.deg2rad(90)), color=pgu.BLUE, vertical=True, font=pgu.mono, text_offset=(80,0), text=str(dist[4]))
    # also show "dist_sum" if available
    dist_sum = state.get('dist_sum')
    if dist_sum:
        pgu.draw.vect(screen, dists_orig, (100, np.deg2rad(dist_sum[0])), vertical=True, color=pgu.YELLOW, font=pgu.mono, text_offset=(0,20), text=str(int(dist_sum[0])))

def draw_headings(screen, compass_orig, r, state):
    pgu.draw.vect(screen, compass_orig, (r, np.deg2rad(state.get('heading', 0))), vertical=True, font=pgu.mono, color=pgu.BLUE)
    pygame.draw.circle(screen, pgu.GRAY, compass_orig, r, 1)
    bno_cal = state.get('bno_cal')
    if bno_cal:
        screen.blit(pgu.sans.render('BNO055 Calibration: ' + str(bno_cal), 1, pgu.GRAY), (compass_orig[0] - r, compass_orig[1] + r + 10))

    # start and set heading (some behaviours use this)
    start_heading = state.get('start_heading')
    if start_heading is not None:
        pgu.draw.vect(screen, compass_orig, (r, np.deg2rad(start_heading)), vertical=True, font=pgu.mono, color=pgu.GRAY, text_offset=(0,40), text='Start: {:>3.0f}'.format(start_heading))
    set_heading = state.get('set_heading')
    if set_heading is not None:
        pgu.draw.vect(screen, compass_orig, (r, np.deg2rad(set_heading)), vertical=True, font=pgu.mono, color=pgu.GREEN, text_offset=(0,20), text='Set: {:>3.0f}'.format(int(round(set_heading))))
