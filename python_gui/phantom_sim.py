#!/usr/bin/env python

"""
Simulation for Phantom: an approximation of the robot's behaviour in simulated 2D environments for the Pi Wars 2019 autonomous challenges.
The idea is to be able to test the actual high level program(s) used to control the robot, while simulating both the environment and the
hardware/microcontroller components. The simulation is not meant to be very accurate, but it should be useful for debugging and for
trying out different algorithms, testing their robustness while tweaking various parameters.

This is work in progress: the simulation is currently a stand-alone mini-project, but it should really run the same actual code that runs
on the robot (e.g. behaviours, the main control loop, etc.), essentially running the "real thing" as much as possible, but replacing all the
sensors and actuators with simulated ones.

In remote control mode, you can either use a game controller (a joystick supported by pygame) or the mouse (by dragging from the middle of the screen).

Interesting things to try:

- Change the behaviours for the challenges, tweak their parameters, or add new ones in behaviours.py
- Change delays in sim_bluepill.py
- Change the layout of the courses or add new ones in phantom_sim.py (here)
- Introduce some randomness to how the motors or sensors behave or add glitches in Robot.update(), in phantom_sim.py (here)

"""

import time, random, pygame, numpy as np, pygame_utils as pgu
from collections import deque
from pygame_utils.scene import Scene, SimplePoly, VectorSprite, VectorSpriteGroup, polys_intersect, find_poly_hits
from control_logic.utils import left_right_motor_speed, pol2cart
from control_logic import motors
from .behaviours import run_remote_ctrl, run_canyons, run_blast_off, run_hubble
from . import sim_bluepill
from .control_panel_utils import draw_motor_graph, draw_dists, draw_headings

MOTOR_DEADBAND = 1
MAX_RPM = 200

class Robot():
    """
    The "physical" simulation of the robot: 
    - draws the robot as VectorSprites on a Scene, and moves it according to 'left_motor_speed' and 'right_motor_speed'
    - detects collisions with the given "walls" and stops movement if any are hit
    - also shows the simulated distance measurements from sensors as blue "beams"
    - can also show two additional "beams" (yellow and green) to indicate some additional vectors (e.g. a set direction)
    """
    def __init__(self, walls, scene, position, direction):
        self.direction = direction
        self.yellow_dir = (0, 0)
        self.green_dir = (0, 0)
        self.position = np.array(position)
        self.left_motor_speed = 0
        self.right_motor_speed = 0
        self.walls = walls
        self.in_collision = False

        w = 0.18
        l = 0.20        
        self.body = VectorSprite([ (-w/4,-l/2-l/8),(w/4,-l/2-l/8),(w/4,-l/2),(w/2,-l/2),(w/2,l/2),(-w/2,l/2),(-w/2,-l/2),(-w/4,-l/2) ], pgu.RED)

        self.left_side_beam = VectorSprite([ (0,0), (0,2) ], pgu.BLUE)
        self.left_side_beam.rotate(np.deg2rad(180-90))
        self.left_side_beam.translate((0,-l/2))

        self.left_beam = VectorSprite([ (0,0), (0,2) ], pgu.BLUE)
        self.left_beam.rotate(np.deg2rad(180-45))
        self.left_beam.translate((0,-l/2))

        self.right_beam = VectorSprite([ (0,0), (0,2) ], pgu.BLUE)
        self.right_beam.rotate(np.deg2rad(180+45))
        self.right_beam.translate((0,-l/2))

        self.right_side_beam = VectorSprite([ (0,0), (0,2) ], pgu.BLUE)
        self.right_side_beam.rotate(np.deg2rad(180+90))
        self.right_side_beam.translate((0,-l/2))

        self.center_beam = VectorSprite([ (0,0), (0,2) ], pgu.BLUE)
        self.center_beam.rotate(np.deg2rad(180))
        self.center_beam.translate((0,-l/2))

        self.dist_beams = [self.left_side_beam, self.left_beam, self.center_beam, self.right_beam, self.right_side_beam]
        
        self.green_beam = VectorSprite([ (0,0), (0,0) ], pgu.GREEN)
        self.green_beam.rotate(np.deg2rad(180))
        self.green_beam.translate((0,-l/2))

        self.yellow_beam = VectorSprite([ (0,0), (0,2) ], pgu.YELLOW)
        self.yellow_beam.rotate(np.deg2rad(180))
        self.yellow_beam.translate((0,-l/2))

        self.group = VectorSpriteGroup([self.body, self.green_beam, self.yellow_beam] + self.dist_beams)
        self.group.rotate(np.deg2rad(direction))
        self.group.move(self.position)
        
        scene.add(self.body)
        for beam in self.dist_beams:
            scene.add(beam)
        scene.add(self.green_beam)
        scene.add(self.yellow_beam)
        
        self.distances = np.zeros(len(self.dist_beams)) * 100
        self.last_time = time.time()
        self.dist_on = True

    def collision(self, movement, turn):
        a = np.deg2rad(turn)
        points = self.group.tr_sprite(self.body, a, movement)
        for i in range(-1, len(points)-1):
            line = (points[i], points[i+1])
            if polys_intersect(self.walls, line):
                return True
        return False

    def distance_measurement(self, dist_on):
        self.dist_on = dist_on
        for b in self.dist_beams:
            b.color = pgu.BLUE if dist_on else None
        
    def update(self):
        """
        This needs to be called regularly to update the robot's position and heading based on the set motor speeds.
        """
        dt = time.time() - self.last_time
        self.last_time = time.time()

        # Calculate simulated RPM from the set motor speeds (-10..10):
        left_rpm, right_rpm = round(MAX_RPM * self.left_motor_speed / 10), round(MAX_RPM * self.right_motor_speed / 10)
        self.left_rpm, self.right_rpm = left_rpm, right_rpm
        # Calculate actual speed from RPM (moving in "self.direction"): m/s
        speed = (left_rpm + right_rpm) / MAX_RPM / 4
        # Calculate turn speed: angle/s
        turn_speed = (left_rpm - right_rpm) * 40 / MAX_RPM
        # Calculate current direction:
        direction = self.direction + turn_speed*dt
        # Calculate movement:
        movement = speed*dt
        # Calculate x, y movement from "movement" and "direction":
        (dx, dy) = pol2cart((movement, np.deg2rad(direction - 90)))
        # Check collision using 2x movement:
        (cdx, cdy) = pol2cart((movement * 2, np.deg2rad(direction - 90)))
        if self.collision((cdx, cdy), turn_speed*dt):
            self.body.color = pgu.WHITE
        else:
            self.direction = direction
            # Update sprite direction and position:
            self.group.rotate(np.deg2rad(direction))
            self.group.translate((dx, dy))
            self.body.color = pgu.RED

        for i, beam in enumerate(self.dist_beams):
            beam.scale(1.0)
            self.group._update_sprite_points()
            h = find_poly_hits(self.walls, beam.points)        
            if h is not None:
                if self.dist_on:
                    beam.color = pgu.BLUE
                    self.distances[i] = h[0]
                    beam.scale(h[0]/2)
                else:
                    self.distances[i] = 100
            else:            
                beam.color = None
                self.distances[i] = 100

        self.yellow_beam.orig_points[1] = pol2cart(self.yellow_dir)
        self.yellow_beam.rotate(np.deg2rad(-90))
        self.green_beam.orig_points[1] = pol2cart(self.green_dir)
        self.green_beam.rotate(np.deg2rad(-90))
        self.group._update_sprite_points()

RPM_XRANGE = 250
MAX_POWER = 4096
left_rpm_hist = deque(maxlen=RPM_XRANGE)
left_set_rpm_hist = deque(maxlen=RPM_XRANGE)
left_power_hist = deque(maxlen=RPM_XRANGE)
right_rpm_hist = deque(maxlen=RPM_XRANGE)
right_set_rpm_hist = deque(maxlen=RPM_XRANGE)
right_power_hist = deque(maxlen=RPM_XRANGE)
def update_screen(scene, state, paused, remote_ctrl_on, start_behaviour, robot):
    # redraw the whole scene:
    scene.draw()

    screen = scene.screen
    
    pgu.draw.table(screen, (0, 0), [
        [
            'F1','Blast Off',
            'F2','Canyons of Mars',
            'F3','Hubble',
            'R', 'Remote Ctrl',
            'P', 'Pause',
            'S', 'Step',
            'Q', 'Quit'
        ]
    ], cols = [
        {'emwidth': 2, 'align': pgu.RIGHT, 'color': pgu.WHITE, 'background': pgu.LIGHT_GRAY}, {'emwidth': 12, 'color': pgu.LIGHT_GRAY, 'background': pgu.YELLOW if start_behaviour == run_blast_off else pgu.DARK_GRAY, 'emhgap': 2},
        {'emwidth': 2, 'align': pgu.RIGHT, 'color': pgu.WHITE, 'background': pgu.LIGHT_GRAY}, {'emwidth': 12, 'color': pgu.LIGHT_GRAY, 'background': pgu.YELLOW if start_behaviour == run_canyons else pgu.DARK_GRAY, 'emhgap': 2},
        {'emwidth': 2, 'align': pgu.RIGHT, 'color': pgu.WHITE, 'background': pgu.LIGHT_GRAY}, {'emwidth': 12, 'color': pgu.LIGHT_GRAY, 'background': pgu.YELLOW if start_behaviour == run_hubble else pgu.DARK_GRAY, 'emhgap': 2},
        {'emwidth': 2, 'align': pgu.RIGHT, 'color': pgu.WHITE, 'background': pgu.LIGHT_GRAY}, {'emwidth': 12, 'color': pgu.LIGHT_GRAY, 'background': pgu.YELLOW if remote_ctrl_on else pgu.DARK_GRAY, 'emhgap': 2},
        {'emwidth': 2, 'align': pgu.RIGHT, 'color': pgu.WHITE, 'background': pgu.LIGHT_GRAY}, {'emwidth': 12, 'color': pgu.LIGHT_GRAY, 'background': pgu.YELLOW if paused else pgu.DARK_GRAY, 'emhgap': 2},
        {'emwidth': 2, 'align': pgu.RIGHT, 'color': pgu.WHITE, 'background': pgu.LIGHT_GRAY}, {'emwidth': 12, 'color': pgu.LIGHT_GRAY, 'background': pgu.DARK_GRAY, 'emhgap': 2},
        {'emwidth': 2, 'align': pgu.RIGHT, 'color': pgu.WHITE, 'background': pgu.LIGHT_GRAY}, {'emwidth': 12, 'color': pgu.LIGHT_GRAY, 'background': pgu.DARK_GRAY, 'hgap': 12}
    ], hgap = 0, hpad = 4, vpad = 6, color = pgu.WHITE, background = pgu.DARK_GRAY)
    
    draw_motor_graph(screen, (20, 50), (RPM_XRANGE, 200), state, left_rpm_hist, right_rpm_hist, left_set_rpm_hist, right_set_rpm_hist, left_power_hist, right_power_hist, MAX_RPM, MAX_POWER, append = not paused)

    draw_dists(screen, (720, 150), 100, state)

    draw_headings(screen, (910, 100), 50, state)

    screen.fill(pygame.Color(80, 80, 80), (1000, 40, 400, 1000), pygame.BLEND_RGBA_SUB)
    pgu.draw.table(screen, (1010, 50), [ [msg] for msg in state['msgs'] ], cols = [ { 'font': pgu.mono } ], color = pgu.GREEN)
    
def fuzz(points, f):
    return points if f == 0 else [(p[0] + random.random() * f - f/2, p[1] + random.random() * f - f/2) for p in points]
    
def canyons(screen):
    scene = Scene(screen, 4, (20, 320))
    scene.zoom(200)

    f = 0
    maze1 = SimplePoly(fuzz([ (3.28,1.71),(3.28,0),(0,0),(0,1.71),(3.28-1.293,1.71) ], f), width = 4, closed = False)
    maze2 = SimplePoly(fuzz([ (3.28-1.972,0),(3.28-1.972,0.541) ], f), width = 4, closed = False)
    maze3 = SimplePoly(fuzz([ (3.28-0.611,1.71),(3.28-0.611,1.71-1.246),(3.28-1.293,1.71-1.246),(3.28-1.293,1.71-0.569),(3.28-2.651,1.71-0.569),(3.28-2.651,1.71-1.245) ], f), width = 4, closed = False)

    scene.add(maze1)
    scene.add(maze2)
    scene.add(maze3)
    return (scene, Robot([maze1, maze2, maze3], scene, (3.0, 1.7), 0), run_canyons)

def blastoff(screen):
    scene = Scene(screen, 4, (20, 320))
    scene.zoom(200)
    
    line = np.array([ (0,0), (2.33,0), (2.33+0.71,0.55), (2.33+0.71*2,0.55), (2.33+0.71*3,0), (2.33*2+0.71*3,0) ])
    top = SimplePoly(line, width = 4, closed = False)
    bottom = SimplePoly(line + np.array([0,0.55]), width = 4, closed = False)
    scene.add(top)
    scene.add(bottom)
    return (scene, Robot([top, bottom], scene, (0.1, 0.25), 90), run_blast_off)

def hubble(screen):
    scene = Scene(screen, 4, (20, 320))
    scene.zoom(300)
    
    box = SimplePoly([ (0,0), (1.22,0), (1.22,1.22), (0,1.22) ], width = 4)
    scene.add(box)
    return (scene, Robot([box], scene, (1.22/2, 1.22/2), 90), run_hubble)

def subscribe_state_changes(state, listeners):
    subs = {}
    for prop, listener in listeners.items():
        subs[prop] = [state.get(prop), listener]
    def update():
        for prop in subs:
            new_value = state.get(prop)
            sub = subs[prop]
            if sub[0] != new_value:
                sub[1](sub[0], new_value)
                sub[0] = new_value
    return update

def main():
    screen = pygame.display.set_mode((1400, 800), 0, 32)
    pygame.display.set_caption('M.E.T. Phantom Simulation')
    clock = pygame.time.Clock()
    paused = True
    remote_ctrl_on = False
    state = {
        'msgs': deque(maxlen = 40),
        'speed': 0,
        'dir': 0,
        'joy_presses': {}
    }

    def distance_measurement_change(old_value, new_value):
        state['msgs'].append('- distance measurements {}'.format('on' if new_value else 'off'))
        sim_bluepill.distance_measurement(new_value)

    def camera_change(old_value, new_value):
        state['msgs'].append('- camera {}'.format('on' if new_value else 'off'))

    def start_scenario(scenario):
        nonlocal scene, robot, start_behaviour, behaviour, remote_ctrl_on, paused
        scene, robot, start_behaviour = scenario(screen)
        behaviour = start_behaviour()
        next(behaviour)
        sim_bluepill.robot = robot
        remote_ctrl_on = False
        paused = True
        state['speed'] = 0
        state['dir'] = 0
        robot.update()
        update_change_listeners()
        
    update_change_listeners = subscribe_state_changes(state, {
        'dist_measurements_on': distance_measurement_change,
        'camera_on': camera_change
    })
    
    lrmotors = [motors.SafeMotor(MOTOR_DEADBAND), motors.SafeMotor(MOTOR_DEADBAND)]
    
    pygame.joystick.init()
    if pygame.joystick.get_count() > 0:
        joystick = pygame.joystick.Joystick(0)
        joystick.init()
    else:
        joystick = None

    scene, robot, start_behaviour = blastoff(screen)
    behaviour = start_behaviour()
    next(behaviour)
    robot.update()    
    sim_bluepill.start(robot)

    update_change_listeners()

    end = False
    while not end:
        step = False
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                end = True
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_q:
                    end = True
                if event.key == pygame.K_p:
                    paused = not paused
                    if not paused:
                        # reset time to avoid jumps
                        robot.last_time = time.time()

                if event.key == pygame.K_F1:
                    start_scenario(blastoff)
                if event.key == pygame.K_F2:
                    start_scenario(canyons)
                if event.key == pygame.K_F3:
                    start_scenario(hubble)
                        
                if event.key == pygame.K_r:
                    remote_ctrl_on = not remote_ctrl_on
                    if remote_ctrl_on:
                        behaviour = run_remote_ctrl()
                        next(behaviour)
                    else:
                        behaviour = start_behaviour()
                        next(behaviour)
                    update_change_listeners()

                if event.key == pygame.K_s and paused:
                    step = True
            if event.type == pygame.JOYBUTTONDOWN and event.button == 1 or event.type == pygame.MOUSEBUTTONDOWN and event.button == 4:
                scene.zoom(min(500, scene.scale + 2))
            elif event.type == pygame.JOYBUTTONDOWN and event.button == 2 or event.type == pygame.MOUSEBUTTONDOWN and event.button == 5:
                scene.zoom(max(50, scene.scale - 2))

        if joystick is None:
            pressed = pygame.mouse.get_pressed()
            if pressed[0]:
                (mx, my) = pygame.mouse.get_pos()
                (cx, cy) = (screen.get_width()/2, screen.get_height()/2)
                state['orig_joy_x'] = min(1, max(-1, (mx - cx)/200))
                state['orig_joy_y'] = min(1, max(-1, (cy - my)/200))
            else:
                state['orig_joy_x'] = 0
                state['orig_joy_y'] = 0
        else:
            state['orig_joy_x'] = joystick.get_axis(2)
            state['orig_joy_y'] = -joystick.get_axis(3)
            
        bluepill_update = sim_bluepill.get_last_update()
        while bluepill_update is not None:
            state.update(bluepill_update)
            bluepill_update = sim_bluepill.get_last_update()

        if not paused or step:
            if step:
                robot.last_time = time.time() - 0.1
            # update extra directions shown by robot:
            robot.yellow_dir = state.get('dist_sum', (0, 0))
            set_heading = state.get('set_heading')
            robot.green_dir = (0, 0) if not set_heading else (100, np.deg2rad((set_heading - robot.direction) % 360))
            # make the robot update its position and rotation:
            robot.update()
            state['heading'] = (robot.direction + 360) % 360
            t = time.monotonic()
            (state['left'], state['right']) = left_right_motor_speed(state['speed'], state['dir'])
            (left_p, right_p) = lrmotors[0].update(t, state['left']), lrmotors[1].update(t, state['right'])
            sim_bluepill.drive_motors(left_p, right_p)
            # run current behaviour:
            if behaviour is not None:
                try:
                    behaviour.send(state)
                    update_change_listeners()
                except StopIteration:
                    behaviour = None
            
        screen.fill(pgu.BLACK)
        update_screen(scene, state, paused and not step, remote_ctrl_on, start_behaviour, robot)
        pygame.display.update()
            
        clock.tick(30)

if __name__ == '__main__':        
    main()
