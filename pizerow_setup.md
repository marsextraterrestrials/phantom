<!-- -*- eval: (typo-mode 1); eval: (flyspell-mode 1); -*- -->

# Configuring the Pi Zero W

This is a recipe for setting up a Raspberry Pi Zero W from scratch, to be used in a robot for [Pi Wars](https://piwars.org/). The focus for now is networking configuration, without anything robot- or other hardware-specific (yet!). 

The main goal is to have the most flexible set of connectivity options:

- At home, the Pi automatically connects to your Wi-Fi network, so it can talk to any other device there, access the internet to install packages, etc.

- When you are somewhere else (e.g. at the competition) with the robot and your laptop, you just connect to **the robot’s own Wi-Fi network** from your laptop, so that you can control or monitor it. (No need for internet or any extra piece of hardware in this case.)

- When all else fails, you can connect your laptop with a USB cable and it will work as a wired Ethernet connection. (Especially useful when changing / experimenting with Wi-Fi settings!)

The AP configuration is mainly based on this excellent how-to: https://blog.thewalr.us/2017/09/26/raspberry-pi-zero-w-simultaneous-ap-and-managed-mode-wifi/



## Prepare the image

1. Download the lite image (e.g. `2018-06-27-raspbian-stretch-lite.zip`) from [raspberrypi.org/downloads/raspbian](https://www.raspberrypi.org/downloads/raspbian/) and unzip it
2. Copy the image to the SD card (from the [Installation Guide](https://www.raspberrypi.org/documentation/installation/installing-images/linux.md)):

        dd bs=4M if=2018-06-27-raspbian-stretch-lite.img of=<device, e.g. /dev/sdb> conv=fsync

3. Enable remote access by setting up the connection to your home Wi-Fi and enabling SSH (see [the docs about headless configuration](https://www.raspberrypi.org/documentation/configuration/wireless/headless.md))

   Add `/boot/wpa_supplicant.conf`:

        ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
        update_config=1
        country=GB

        network={
            ssid="<Home Wi-Fi SSID>"
            psk="<Home Wi-Fi passphrase>"
            key_mgmt=WPA-PSK
            id_str="AP1"
        }
            
   Add an empty `/boot/ssh` file.

4. Insert the SD card and turn on the Pi.

5. Find the IP address (e.g. from your router) and SSH using the standard pi/raspberry credentials, e.g.:

        ssh pi@10.0.1.101



## Basic settings and initial update

After the first boot:

1. Change the hostname in `/etc/hostname` – and also in `/etc/hosts`:

        127.0.1.1       <hostname for your Pi>


1. Change the default password (`passwd`)

1. Add your SSH public keys:

        mkdir .ssh
        nano .ssh/authorized_keys  # add your keys here
        chmod -R go= .ssh

1. Expand the filesystem (if necessary): run `sudo raspi-config`, select Advanced Options → Expand Filesystem, then reboot for the changes to take effect..

1. Update all packages:

        apt-get update && apt-get dist-upgrade

1. Install some essential packages:

        apt-get install rng-tools i2c-tools python3-smbus python3-pip virtualenv python3-virtualenv
        
   (`rng-tools` is needed to have enough entropy available for the AP to work!)

1. Configure things with `raspi-config`:

        Interfacing Options → P5 I2C → Yes

        Interfacing options → Serial → disable the console and enable the serial port (so that `/dev/ttyS0` will work)

        Localisation Options → Change Timezone



## USB On-The-Go Ethernet

This allows you to connect the Pi to your computer via USB as if it was an Ethernet device, using a fixed IP address. This can be very useful if Wi-Fi doesn’t work for some reason.

References: 

- https://medium.com/@matthewhuie/connecting-a-raspberry-pi-zero-to-a-network-via-usb-on-the-go-and-openwrt-8995a58ec151
- https://learn.adafruit.com/turning-your-raspberry-pi-zero-into-a-usb-gadget/ethernet-gadget

We are setting up a new network (`192.168.7.x`), with a fixed IP address for the Pi: `192.168.7.1`.

1. Add this to `/boot/config.txt`:

        # To enable USB gadget mode for Ethernet over USB
        dtoverlay=dwc2
        
1. Add this to `/etc/modules`:

        # Ethernet over USB
        dwc2
        g_ether

1. Add this to the end of `/etc/network/interfaces`:

        # Ethernet over USB
        allow-hotplug usb0
        iface usb0 inet static
            address 192.168.7.2
            netmask 255.255.255.0
            network 192.168.7.0
            broadcast 192.168.7.255

1. Reboot

1. Connect the Pi to your laptop with a USB cable. (The Pi Zero can also be powered from the USB data port without a separate power cable. But if for some reason you can’t SSH to it, you won’t be able to shut it down cleanly!)

1. On the laptop:

   A new Ethernet device should appear, for example:

        # ip a
        
        […]

        5: enp0s20u1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
            link/ether 66:bb:2a:35:7a:2b brd ff:ff:ff:ff:ff:ff
            inet6 fe80::64bb:2aff:fe35:7a2b/64 scope link 
               valid_lft forever preferred_lft forever

    You could assign an IP address to this interface manually now (`ip addr add 192.168.7.3/24 dev enp0s20u1`), and `ssh 129.168.7.1` to connect to the Pi now, but see the next section to make this easier.



## Access Point Mode

This is a somewhat unusual setup, as we need the AP to work in a “standalone” mode: the Pi doesn’t need to be connected to any other network and we only use the AP to connect to the Pi itself. (The “usual” setup is when you want to use the Pi as a router: it’s connected to an existing network (internet) somehow, either via Ethernet or Wi-Fi, and you want to create another Wi-Fi network where clients can connect to and then you route traffic between the two.)

1. Install `hostapd`:

        apt-get install hostapd

1. Create `/etc/udev/rules.d/70-persistent-net.rules`:

        SUBSYSTEM=="ieee80211", ACTION=="add|change", ATTR{macaddress}=="<MAC address of your wlan0>", KERNEL=="phy0", \
          RUN+="/sbin/iw phy phy0 interface add ap0 type __ap", \
          RUN+="/bin/ip link set ap0 address b8:27:eb:ff:ff:ff"

   (Use your real mac address, e.g. as shown by `iw dev`.)
 
1. Add to `/etc/hostapd/hostapd.conf`:

        ctrl_interface=/var/run/hostapd
        ctrl_interface_group=0
        interface=ap0
        ssid=<SSID: the name of the new Wi-Fi network>
        hw_mode=g
        channel=11
        wmm_enabled=0
        macaddr_acl=0
        auth_algs=1
        wpa=2
        wpa_passphrase=<passphrase, at least 8 chars>
        wpa_key_mgmt=WPA-PSK
        wpa_pairwise=TKIP CCMP
        rsn_pairwise=CCMP

1. Add to `/etc/default/hostapd`:

        DAEMON_CONF="/etc/hostapd/hostapd.conf"

1. Modify `/etc/network/interfaces`: add the new `ap0` interface and make sure `wlan` is not started automatically:

        auto lo
        auto ap0
        iface lo inet loopback

        # AP setup (must come before wlan0!)
        allow-hotplug ap0
        iface ap0 inet static
            address 192.168.8.1
            netmask 255.255.255.0

        # Connect to home wifi network if available (_not_ automatic)
        iface wlan0 inet dhcp
            wpa-conf /etc/wpa_supplicant/wpa_supplicant.conf

1. We also need a hack to ensure `wlan0` starts *after* `ap0` (if we started both `ap0` and `wlan0` with `auto` in `/etc/network/interfaces/` this wouldn’t be guaranteed). Add this at the end of `/etc/rc.local` (before the final `exit 0` line):

        logger "Starting wlan0"
        /sbin/ifup wlan0



## dnsmasq: DHCP and DNS

When connecting to the AP or via USB, you could set up the IP address for the network interface on your machine manually, but it’s much nicer if the Pi takes care of this too.

1. Install `dnsmasq`:

        apt-get install dnsmasq

2. Add this to `/etc/dnsmasq.conf`:

        # Only listen on these interfaces:
        interface=lo,ap0,usb0
        # Don't do DHCP for localhost 
        no-dhcp-interface=lo
        domain-needed
        bogus-priv
        # DHCP range for ap0
        dhcp-range=192.168.8.50,192.168.8.150,255.255.255.0,12h
        # DHCP range for usb0
        dhcp-range=192.168.7.50,192.168.7.150,255.255.255.0,12h
        # Don't send default route
        dhcp-option=3
        # Don't use /etc/hosts
        no-hosts
        # Use these instead
        address=/<hostname-for-AP>/192.168.8.1
        address=/<hostname-for-USB>/192.168.7.1

The last two lines are optional, as you can always use the known IP addresses to connect, but it’s nice to have the Pi provide DNS too when using in “standalone” mode. With all this in place, when your laptop is not connected to the internet (or any other network with DNS), all you need to do is connect to the Pi’s Wi-Fi, and `ssh hostname-for-AP`. (Your laptop would get an IP address assigned from the range specified above, and the Pi would be set up as a name server to resolve its own hostname. Same for the USB connection.)

## Additional, hardware-specific settings

The following are specific to Phantom and additional hardware being used.

### Reduce I2C baud rate

Add this to `/boot/config.txt`:

    # Reduce i2c baud rate – as a workaround for BNO055 – see https://github.com/ghirlekar/bno055-python-i2c
    dtparam=i2c_baudrate=50000

# Misc. notes

## SD card backup, copy and clone

To make a backup of the SD card (`/dev/sdx`):

    dd bs=1M if=/dev/sdx | gzip > /path/to/image_backup.gz

To restore it – or to make a clone using an identical card:

    gzip -dc /path/to/image_backup.gz | dd bs=1M of=/dev/sdx

